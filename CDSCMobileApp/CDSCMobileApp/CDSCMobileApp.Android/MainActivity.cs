﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Graphics.Drawables;
using Android.Graphics;
using Android.Support.V7.App;
using System.Net;
using System.IO;

namespace CDSCMobileApp.Droid
{
	[Activity ( Label = "CDSC MOBILE",Theme = "@style/AppTheme2",  Icon = "@drawable/ctrademobile")]
	public class MainActivity : AppCompatActivity
	{
		int count = 1;
        LinearLayout llPriceNotification, llTopGainers, llTopLosers, llTradingAccount, llChangePassword, llDeactivateAccount;


        protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
 //ActionBar.SetBackgroundDrawable(new ColorDrawable(Color.ParseColor("#1e1d23")));
           LayoutInflater inflater = LayoutInflater.From(this);
            // Set our view from the "main" layout resource
            SetContentView (Resource.Layout.Main);
            // Get our button from the layout resource,
            // and attach an event to it
            //Button button = FindViewById<Button> (Resource.Id.myButton);
            llPriceNotification = (LinearLayout)FindViewById(Resource.Id.llPriceNotification);
            llTopGainers = (LinearLayout)FindViewById(Resource.Id.llTopGainers);
            llTopLosers = (LinearLayout)FindViewById(Resource.Id.llTopLosers);
            llTradingAccount = (LinearLayout)FindViewById(Resource.Id.llTradingAccount);
            llChangePassword = (LinearLayout)FindViewById(Resource.Id.llChangePassword);
            llDeactivateAccount = (LinearLayout)FindViewById(Resource.Id.llDeactivateAccount);

            llPriceNotification.Click += LlPriceNotification_Click;
            llTopGainers.Click += LlTopGainers_Click;
            llTopLosers.Click += LlTopLosers_Click;
            llTradingAccount.Click += LlTradingAccount_Click;
            llChangePassword.Click += LlChangePassword_Click;
            llDeactivateAccount.Click += LlDeactivateAccount_Click;
            
        }

        private void LlDeactivateAccount_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(ActivityCDA));

        }

        private void LlChangePassword_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(ActivityMarketWatch));

        }

        private void LlTradingAccount_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(ActivityTradingAccount));
        }

        private void LlTopLosers_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(ActivityTopLosers));

        }

        private void LlTopGainers_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(ActivityTopGainers));
        }

        private void LlPriceNotification_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(ActivityPriceNotification));
        }
        public bool IsAddressAvailable(string address)
        {

            Boolean isHttpOk;
            try
            {
                Stream sStream;
                HttpWebRequest urlReq;
                HttpWebResponse urlRes;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(address);
                req.Method = "HEAD";
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                isHttpOk = resp.StatusCode == HttpStatusCode.OK;
                resp.Close();

                return isHttpOk;
            }
            catch (Exception)
            {
                isHttpOk = false;
                return isHttpOk;
            }
        }
    }
}


