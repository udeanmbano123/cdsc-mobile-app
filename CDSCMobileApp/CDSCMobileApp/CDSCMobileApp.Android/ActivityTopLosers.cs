﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using CDSCMobileeApp.DROID.Fragments;
using Android.Graphics.Drawables;
using Android.Graphics;
using System.Xml.Serialization;
using System.IO;
using RestSharp;
using System.Net;

namespace CDSCMobileApp.Droid
{
    [Activity(Label = "TOP LOSERS", Theme = "@style/AppTheme2", MainLauncher = false, Icon = "@drawable/ctrademobile")]
    public class ActivityTopLosers : AppCompatActivity
    {
        RecyclerView newn;
        RecyclerView.LayoutManager mLayoutManager;
        PriceAdapter mAdapter;
        List<DATASETPRICES> mPhotoAlbum;
        public static string baseurl;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.ActivityPriceNotification);
            newn = this.FindViewById<RecyclerView>(Resource.Id.recyclerView);

               baseurl = "http://197.248.8.104/CDSCW";
            

            try 
	{	        
		 // Plug in the linear layout manager:
            mLayoutManager = new LinearLayoutManager(this);
            newn.SetLayoutManager(mLayoutManager);
            var client = new RestClient(baseurl);
            var request = new RestRequest("dtpricesL", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            if (validate.Contains("dATASET_PRICESs") ==false)
            {
                //validate = "<dATASET_PRICESs><DATASETPRICES><AVERAGE_PRICE>1090</AVERAGE_PRICE><CLOSE_PRICE>1248</CLOSE_PRICE><ISIN>KE0000000356</ISIN><ISSUER_CODE>LIMT</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>1</num_of_trades><PRICE_HI>1090</PRICE_HI><PRICE_LOW>1090</PRICE_LOW><SHARE_VOLUME>100</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>109000</TURNOVER><trade_date>03-07-2015</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>203</AVERAGE_PRICE><CLOSE_PRICE>223</CLOSE_PRICE><ISIN>KE0000000281</ISIN><ISSUER_CODE>KUKZ</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>1</num_of_trades><PRICE_HI>203</PRICE_HI><PRICE_LOW>203</PRICE_LOW><SHARE_VOLUME>700</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>142100</TURNOVER><trade_date>03-02-2015</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>51.5</AVERAGE_PRICE><CLOSE_PRICE>57</CLOSE_PRICE><ISIN>KE0000000265</ISIN><ISSUER_CODE>ICDC</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>36</num_of_trades><PRICE_HI>52.5</PRICE_HI><PRICE_LOW>51</PRICE_LOW><SHARE_VOLUME>41100</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>2135850</TURNOVER><trade_date>01-10-2015</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>275</AVERAGE_PRICE><CLOSE_PRICE>288</CLOSE_PRICE><ISIN>KE0000000505</ISIN><ISSUER_CODE>WTK</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>7</num_of_trades><PRICE_HI>275</PRICE_HI><PRICE_LOW>275</PRICE_LOW><SHARE_VOLUME>33500</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>9212500</TURNOVER><trade_date>02-09-2014</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>108</AVERAGE_PRICE><CLOSE_PRICE>113</CLOSE_PRICE><ISIN>KE0000000273</ISIN><ISSUER_CODE>JUB</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>6</num_of_trades><PRICE_HI>108</PRICE_HI><PRICE_LOW>108</PRICE_LOW><SHARE_VOLUME>10500</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>1134000</TURNOVER><trade_date>02-10-2009</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>68.5</AVERAGE_PRICE><CLOSE_PRICE>72</CLOSE_PRICE><ISIN>KE0000000455</ISIN><ISSUER_CODE>SGL</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>16</num_of_trades><PRICE_HI>69</PRICE_HI><PRICE_LOW>68</PRICE_LOW><SHARE_VOLUME>11172</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>770484</TURNOVER><trade_date>02-02-2007</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>11.15</AVERAGE_PRICE><CLOSE_PRICE>12</CLOSE_PRICE><ISIN>KE0000000174</ISIN><ISSUER_CODE>CABL</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>22</num_of_trades><PRICE_HI>11.7</PRICE_HI><PRICE_LOW>11</PRICE_LOW><SHARE_VOLUME>69400</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>773555</TURNOVER><trade_date>01-09-2015</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>215</AVERAGE_PRICE><CLOSE_PRICE>220</CLOSE_PRICE><ISIN>KE0000000059</ISIN><ISSUER_CODE>BAMB</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>4</num_of_trades><PRICE_HI>215</PRICE_HI><PRICE_LOW>215</PRICE_LOW><SHARE_VOLUME>1089</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>234580</TURNOVER><trade_date>02-03-2007</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>32.25</AVERAGE_PRICE><CLOSE_PRICE>33</CLOSE_PRICE><ISIN>KE1000001802</ISIN><ISSUER_CODE>TPSE</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>1</num_of_trades><PRICE_HI>32.25</PRICE_HI><PRICE_LOW>32.25</PRICE_LOW><SHARE_VOLUME>100</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>3225</TURNOVER><trade_date>03-08-2015</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>32.25</AVERAGE_PRICE><CLOSE_PRICE>33</CLOSE_PRICE><ISIN>KE0000000539</ISIN><ISSUER_CODE>TPSE</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>1</num_of_trades><PRICE_HI>32.25</PRICE_HI><PRICE_LOW>32.25</PRICE_LOW><SHARE_VOLUME>100</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>3225</TURNOVER><trade_date>03-08-2015</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>25.5</AVERAGE_PRICE><CLOSE_PRICE>28</CLOSE_PRICE><ISIN>KE1000001451</ISIN><ISSUER_CODE>HFCK</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>42</num_of_trades><PRICE_HI>27.5</PRICE_HI><PRICE_LOW>25</PRICE_LOW><SHARE_VOLUME>33760</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>898080</TURNOVER><trade_date>02-05-2007</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>25.5</AVERAGE_PRICE><CLOSE_PRICE>28</CLOSE_PRICE><ISIN>KE0000000240</ISIN><ISSUER_CODE>HFCK</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>42</num_of_trades><PRICE_HI>27.5</PRICE_HI><PRICE_LOW>25</PRICE_LOW><SHARE_VOLUME>33760</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>898080</TURNOVER><trade_date>02-05-2007</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>17</AVERAGE_PRICE><CLOSE_PRICE>17.2</CLOSE_PRICE><ISIN>KE0000000224</ISIN><ISSUER_CODE>XPRS</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>5</num_of_trades><PRICE_HI>17</PRICE_HI><PRICE_LOW>17</PRICE_LOW><SHARE_VOLUME>659</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>11216.25</TURNOVER><trade_date>03-09-2008</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>302</AVERAGE_PRICE><CLOSE_PRICE>305</CLOSE_PRICE><ISIN>KE0000000216</ISIN><ISSUER_CODE>EABL</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>11</num_of_trades><PRICE_HI>302</PRICE_HI><PRICE_LOW>302</PRICE_LOW><SHARE_VOLUME>804000</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>242808000</TURNOVER><trade_date>02-01-2015</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>107</AVERAGE_PRICE><CLOSE_PRICE>108</CLOSE_PRICE><ISIN>KE0000000125</ISIN><ISSUER_CODE>I&amp;M</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>5</num_of_trades><PRICE_HI>107</PRICE_HI><PRICE_LOW>107</PRICE_LOW><SHARE_VOLUME>8600</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>920200</TURNOVER><trade_date>01-10-2015</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>62</AVERAGE_PRICE><CLOSE_PRICE>62.5</CLOSE_PRICE><ISIN>KE0000000265</ISIN><ISSUER_CODE>ICDC</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>10</num_of_trades><PRICE_HI>62</PRICE_HI><PRICE_LOW>62</PRICE_LOW><SHARE_VOLUME>5968</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>370042</TURNOVER><trade_date>01-03-2005</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>70</AVERAGE_PRICE><CLOSE_PRICE>70.5</CLOSE_PRICE><ISIN>KE0000000638</ISIN><ISSUER_CODE>DTK</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>2</num_of_trades><PRICE_HI>70</PRICE_HI><PRICE_LOW>70</PRICE_LOW><SHARE_VOLUME>700</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>49000</TURNOVER><trade_date>03-02-2010</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>70</AVERAGE_PRICE><CLOSE_PRICE>70.5</CLOSE_PRICE><ISIN>KE0000000158</ISIN><ISSUER_CODE>DTK</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>2</num_of_trades><PRICE_HI>70</PRICE_HI><PRICE_LOW>70</PRICE_LOW><SHARE_VOLUME>700</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>49000</TURNOVER><trade_date>03-02-2010</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>179</AVERAGE_PRICE><CLOSE_PRICE>180</CLOSE_PRICE><ISIN>KE1000001819</ISIN><ISSUER_CODE>SCBK</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>6</num_of_trades><PRICE_HI>179</PRICE_HI><PRICE_LOW>179</PRICE_LOW><SHARE_VOLUME>4400</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>787600</TURNOVER><trade_date>02-11-2011</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>179</AVERAGE_PRICE><CLOSE_PRICE>180</CLOSE_PRICE><ISIN>KE0000000448</ISIN><ISSUER_CODE>SCBK</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>6</num_of_trades><PRICE_HI>179</PRICE_HI><PRICE_LOW>179</PRICE_LOW><SHARE_VOLUME>4400</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>787600</TURNOVER><trade_date>02-11-2011</trade_date></DATASETPRICES></dATASET_PRICESs>";
            }
            //Console.WriteLine(validate);
            dATASET_PRICESs result = null;
            List<dATASET_PRICESs> nn = null;
            XmlSerializer serializer = new XmlSerializer(typeof(dATASET_PRICESs));
            using (TextReader reader = new StringReader(validate))
            {
                try
                {
                    result = (dATASET_PRICESs)serializer.Deserialize(reader);

                }
                catch (Exception)
                {


                }
            }

            Drawable[] imageId = new Drawable[result.DATASETPRICESs.Count];
            int x = 0;
            TextDrawable.TextDrawable drawable;
            foreach (var d in result.DATASETPRICESs)
            {

                try
                {
                    //TextDrawable.TextDrawable drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BuildRound("ABC", GetRandomColor(), GetRandomColor());
                    drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BeginConfig().FontSize(28).TextColor(Color.Black).Bold().EndConfig().BuildRoundRect(d.ISSUER_CODE, GetRandomColor(),2, GetRandomColor());
                    imageId[x] = drawable;
                }
                catch (Exception)
                {
                    continue;
                }
                x += 1;
            }
            mPhotoAlbum = result.DATASETPRICESs;
            int my = mPhotoAlbum.Count;
            // Plug in my adapter:
            mAdapter = new PriceAdapter(mPhotoAlbum, imageId);
            newn.SetAdapter(mAdapter);

	}
	catch (Exception e)
	{
		
		
	}
        }
        static Random rand = new Random();
        public static Color GetRandomColor()
        {
            int hue = rand.Next(255);
            Color color = Color.HSVToColor(
                new[] {
            hue,
            1.0f,
            1.0f,
                }
            );
            return color;
        }
        public bool IsAddressAvailable(string address)
        {

            Boolean isHttpOk;
            try
            {
                Stream sStream;
                HttpWebRequest urlReq;
                HttpWebResponse urlRes;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(address);
                req.Method = "HEAD";
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                isHttpOk = resp.StatusCode == HttpStatusCode.OK;
                resp.Close();

                return isHttpOk;
            }
            catch (Exception)
            {
                isHttpOk = false;
                return isHttpOk;
            }
        }
    }
}