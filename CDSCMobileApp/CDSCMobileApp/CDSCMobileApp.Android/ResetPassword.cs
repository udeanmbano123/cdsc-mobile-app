﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace CDSCMobileApp.Droid
{
    [Activity(Label = "Reset Password", Theme = "@style/AppTheme", Icon = "@drawable/icon")]
    public class ResetPassword : Activity
    {
        EditText emailAddress, new_password, con_pass, nationalId;
        Button sign_up;
        String ip;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.ResetPassword);

            emailAddress = (EditText)FindViewById(Resource.Id.emailAddress);
            new_password = (EditText)FindViewById(Resource.Id.new_pass);
            nationalId = (EditText)FindViewById(Resource.Id.nationalId);
            con_pass = (EditText)FindViewById(Resource.Id.con_pass);



            sign_up = (Button)FindViewById(Resource.Id.sign_up);
            sign_up.Click += Sign_up_Click;
        }

        private void Sign_up_Click(object sender, EventArgs e)
        {
            Toast.MakeText(this, "Reset password", ToastLength.Short).Show();
        }
    }
}