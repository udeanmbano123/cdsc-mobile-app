using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using CDSCMobileApp.Droid.Fragments;
using System.Net;
using System.Net.Sockets;
using System.IO;
using RestSharp;

namespace CDSCMobileApp.Droid
{
    [Activity(Label = "CDSC Mobile App", Theme = "@style/AppTheme2", MainLauncher = false, Icon = "@drawable/ctrademobile")]
    public class UserActivity : AppCompatActivity
    {
        TextView jump;
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        public static string username;
        public static string baseurl;
        public static string member;
        public static string title { get; set; }
        public static string responsemessage { get; set; }
        public static string datereceived { get; set; }
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            member = Intent.GetStringExtra("Member") ?? "Data not available";
            //member = "0000000000450";
            username = Intent.GetStringExtra("Username") ?? "Data not available";
            SetContentView(Resource.Layout.main2);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            //SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_menu);
            drawerLayout = this.FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            navigationView = this.FindViewById<NavigationView>(Resource.Id.nav_view);
            jump = this.FindViewById<TextView>(Resource.Layout.nav_header);
            var headerView = navigationView.GetHeaderView(0);
            jump = headerView.FindViewById<TextView>(Resource.Id.lends);
            baseurl = "http://197.248.8.104/CDSCW";
            UserActivity.title = "Dashboard";
           var client = new RestClient(baseurl);
            var request = new RestRequest("dtnames/{s}", Method.GET);
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("s", UserActivity.member);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            validate = validate.Replace(@"""", "");
            jump.Text = "Client Number :" + validate;

            Intent downloadIntent = new Intent(this, typeof(AppUsageIntent));

            // This is just one example of passing some values to an IntentService via the Intent:
            StartService(downloadIntent);
            //Set hamburger items menu




            string test = baseurl;
            //handle navigation
            navigationView.NavigationItemSelected += (sender, e) =>
            {
                e.MenuItem.SetChecked(true);

                switch (e.MenuItem.ItemId)
                {
                    case Resource.Id.nav1:
                        ListItemClicked(0);
                        break;
                    case Resource.Id.nav2:
                        ListItemClicked(1);
                        break;
                    case Resource.Id.nav3:
                        ListItemClicked(2);
                        break;
                    case Resource.Id.nav4:
                        ListItemClicked(3);
                        break;
                    case Resource.Id.nav5:
                        ListItemClicked(4);
                        break;
                    case Resource.Id.nav6:
                        ListItemClicked(5);
                        break;
                    case Resource.Id.nav7:
                        ListItemClicked(6);
                        break;
                    case Resource.Id.nav8:
                        ListItemClicked(7);
                        break;
                    case Resource.Id.nav9:
                        ListItemClicked(8);
                        break;
                    case Resource.Id.nav10:
                        ListItemClicked(9);
                        break;
                    case Resource.Id.nav11:
                        ListItemClicked(10);
                        break;
                    case Resource.Id.nav12:
                        ListItemClicked(11);
                        break;
                    case Resource.Id.nav13:
                        ListItemClicked(12);
                        break;
                    case Resource.Id.nav14:
                        ListItemClicked(13);
                        break;
                    case Resource.Id.home:
                        ListItemClicked(14);
                        break;
                    case Resource.Id.nav15:
                        ListItemClicked(15);
                        break;
                }

                try
                {
                    Snackbar.Make(drawerLayout, "You selected: " + e.MenuItem.TitleFormatted, Snackbar.LengthLong)
                                .Show();
                }
                catch (System.Exception)
                {

                }
                if (e.MenuItem.TitleFormatted.ToString() == "HOME")
                {
                    Android.Support.V4.App.Fragment fragment = null;
                    SupportFragmentManager.BeginTransaction()
                      .Replace(Resource.Id.content_frame, Fragment1.NewInstance())
                      .Commit();
                }

                drawerLayout.CloseDrawers();
            };


            //if first time you will want to go ahead and click first item.
            //if first time you will want to go ahead and click first item.
            if (bundle == null)
            {

                ListItemClicked(0);
                Android.Support.V4.App.Fragment fragment = null;
                fragment = Fragment1.NewInstance();
                SupportFragmentManager.BeginTransaction()
               .Replace(Resource.Id.content_frame, fragment)
               .Commit();

            }

        }
        int oldPosition = -1;
        private void ListItemClicked(int position)
        {
            //this way we don't load twice, but you might want to modify this a bit.
            if (position == oldPosition)
                return;

            oldPosition = position;

            Android.Support.V4.App.Fragment fragment = null;
            switch (position)
            {
                case 14:
                    fragment = Fragment1.NewInstance();
                    break;
                case 0:
                    fragment = Escrow.NewInstance();
                    break;
                case 1:
                    fragment = Enquiry.NewInstance();
                    break;
                case 2:
                    fragment =TopGainers.NewInstance();
                    break;
                case 3:

                    fragment = TopLosers.NewInstance();

                    break;
                case 4:

                    fragment = Issuer.NewInstance();

                    break;
                case 5:
                    {
                      
                        fragment = Price.NewInstance();
                    }
                    break;
                case 6:

                    fragment = Holdings.NewInstance();

                    break;
                case 7:

                    fragment = Statement.NewInstance();

                    break;
                case 8:

                    fragment = Actions.NewInstance();

                    break;
                case 9:

                    fragment = Payment.NewInstance();

                    break;
                case 10:

                    fragment = ChangePassword.NewInstance();

                    break;
                case 11:
                    fragment = ProfileUpdate.NewInstance();
                    break;
                case 12:
                    fragment = Deactivate.NewInstance();
                    break;
                case 13:
                    {
                       Finish();
                      StartActivity(typeof(MainActivity));
                    }
                    break;
                case 15:

                    fragment = CDA.NewInstance();

                    break;
            }

            try
            {
                SupportFragmentManager.BeginTransaction()
                   .Replace(Resource.Id.content_frame, fragment).Detach(fragment).Attach(fragment)
                   .Commit();
            }
            catch (System.Exception)
            {
               
            }
        }
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    drawerLayout.OpenDrawer(Android.Support.V4.View.GravityCompat.Start);
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }
        public bool IsAddressAvailable(string address)
        {
            Boolean isHttpOk;
            try
            {
                Stream sStream;
                HttpWebRequest urlReq;
                HttpWebResponse urlRes;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(address);
                req.Method = "HEAD";
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                 isHttpOk = resp.StatusCode == HttpStatusCode.OK;
                resp.Close();

                return isHttpOk;
            }
            catch (Exception)
            {
                isHttpOk = false;
                return isHttpOk;
            }

        }
    }
}