package md5df477e27c285d1ed47b6e83e7f8e9de0;


public class ForgotActivity
	extends android.support.v7.app.AppCompatActivity
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("CDSCMobileApp.Activities.ForgotActivity, CDSCMobileApp.Android, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", ForgotActivity.class, __md_methods);
	}


	public ForgotActivity () throws java.lang.Throwable
	{
		super ();
		if (getClass () == ForgotActivity.class)
			mono.android.TypeManager.Activate ("CDSCMobileApp.Activities.ForgotActivity, CDSCMobileApp.Android, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
