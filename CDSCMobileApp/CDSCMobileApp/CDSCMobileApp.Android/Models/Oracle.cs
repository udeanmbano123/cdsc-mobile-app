﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace CDSCMobileApp
{
  
    [XmlRoot("users")]
    public class users
    {
        [XmlElement("user")]
        public List<user> userss { get; set; }
    }
     public class user
    {
        [XmlElement("id")]
        public string id { get; set; }
        [XmlElement("name")]
        public string name { get; set; }
        [XmlElement("profession")]
        public string profession { get; set; }

    }
    [XmlRoot("dATASET_CDAs")]
    public class dATASET_CDAs
    {
        [XmlElement("DATASETCDA")]
        public List<DATASETCDA> DATASETCDAs { get; set; }

    }
    public class DATASETCDA
    {
        [XmlElement("MEMBER_CODE")]
        public string MEMBER_CODE{ get; set; }
        [XmlElement("MEMBER_TYPE")]
        public string MEMBER_TYPE { get; set; }
        [XmlElement("NAME")]
        public string NAME { get; set; }
        [XmlElement("ADDRESS1")]
        public string ADDRESS1 { get; set; }
        [XmlElement("ADDRESS2")]
        public string ADDRESS2 { get; set; }
        [XmlElement("ADDRESS3")]
      public string ADDRESS3 { get; set; }
        [XmlElement("TOWN")]
        public string TOWN { get; set; }
        [XmlElement("POST_CODE")]
        public string POST_CODE { get; set; }
        [XmlElement("COUNTRY_CODE")]
        public string COUNTRY_CODE { get; set; }
        [XmlElement("TELEPHONE")]
        public string TELEPHONE { get; set; }
        [XmlElement("fax")]
        public string fax { get; set; }
        [XmlElement("E_MAIL")]
        public string E_MAIL { get; set; }
        [XmlElement("STATUS")]
        public string STATUS { get; set; }
        [XmlElement("status_description")]
        public string status_description { get; set; }
    }

    [XmlRoot("dATASET_HOLDINGSs")]
    public class dATASET_HOLDINGSs
    {
        [XmlElement("DATASETHOLDINGS")]
        public List<DATASETHOLDINGS> DATASETCDAs { get; set; }

    }


    public class DATASETHOLDINGS
    {
        [XmlElement("CLIENT_PREFIX")]
        public string CLIENT_PREFIX { get; set; }
        [XmlElement("CLIENT_SUFFIX")]
        public string CLIENT_SUFFIX { get; set; }
        [XmlElement("MEMBER_CODE")]
        public string MEMBER_CODE { get; set; }
        [XmlElement("MEMBER_TYPE")]
        public string MEMBER_TYPE { get; set; }
        [XmlElement("SHORT_NAME")]
        public string SHORT_NAME { get; set; }
        [XmlElement("ISSUER_CODE")]
        public string ISSUER_CODE { get; set; }
        [XmlElement("MAIN_TYPE")]
        public string MAIN_TYPE { get; set; }
        [XmlElement("SUB_TYPE")]
        public string SUB_TYPE { get; set; }
        [XmlElement("qnty_held")]
        public string qnty_held { get; set; }
        [XmlElement("DATE_LAST_TRANSACTION")]
        public string DATE_LAST_TRANSACTION { get; set; }
        [XmlElement("sec_ACC_TYPE")]
        public string sec_ACC_TYPE { get; set; }
    }

    [XmlRoot("dATASET_ISSUERSs")]
    public class dATASET_ISSUERSs
    {
        [XmlElement("DATASETISSUERS")]
        public List<DATASETISSUERS> DATASETCDAs { get; set; }

    }

    public class DATASETISSUERS
    {
        [XmlElement("NAME")]
        public string NAME { get; set; }
        [XmlElement("ISSUER_CODE")]
        public string ISSUER_CODE { get; set; }
        [XmlElement("MAIN_TYPE")]
        public string MAIN_TYPE { get; set; }
        [XmlElement("SUB_TYPE")]
        public string SUB_TYPE { get; set; }
        [XmlElement("ISIN")]
        public string ISIN { get; set; }
        [XmlElement("SHORT_NAME")]
        public string SHORT_NAME { get; set; }
        [XmlElement("FH_LIMIT")]
        public string FH_LIMIT { get; set; }
        [XmlElement("FOREIGN_HOLDING")]
        public string FOREIGN_HOLDING { get; set; }
        [XmlElement("PRICE_LAST_TRADED")]
        public string PRICE_LAST_TRADED { get; set; }
        [XmlElement("COUNTRY_OF_INITIAL_ISSUE")]
        public string COUNTRY_OF_INITIAL_ISSUE { get; set; }
        [XmlElement("PAR_VALUE")]
        public string PAR_VALUE { get; set; }
        [XmlElement("DATE_LAST_TRADED")]
        public string DATE_LAST_TRADED { get; set; }
        [XmlElement("QTY_ISSUED")]
        public string QTY_ISSUED { get; set; }
        [XmlElement("qty_demmarted")]
        public string qty_demmarted { get; set; }
        [XmlElement("PREVIOUS_CLOSE")]
        public string PREVIOUS_CLOSE { get; set; }
        [XmlElement("INDEXED_PRICE")]
        public string INDEXED_PRICE { get; set; }
        [XmlElement("PREVIOUS_INDEXED_PRICE")]
        public string PREVIOUS_INDEXED_PRICE { get; set; }
        [XmlElement("DATE_LAST_INDEXED")]
        public string DATE_LAST_INDEXED { get; set; }
        [XmlElement("INDEXED_QUANTITY")]
        public string INDEXED_QUANTITY { get; set; }
        [XmlElement("AVERAGE_PRICE")]
        public string AVERAGE_PRICE { get; set; }
        [XmlElement("SECURITY_CATEGORY")]
        public string SECURITY_CATEGORY { get; set; }
        [XmlElement("REFERENCE_PRICE")]
        public string REFERENCE_PRICE { get; set; }
        [XmlElement("STATUS")]
        public string STATUS { get; set; }
        [XmlElement("status_description")]
        public string status_description { get; set; }
    }
    [XmlRoot("dATASET_PRICESs")]
    public class dATASET_PRICESs
    {
        [XmlElement("DATASETPRICES")]
        public List<DATASETPRICES> DATASETPRICESs { get; set; }

    }

    public class DATASETPRICES
    {
        [XmlElement("ISSUER_CODE")]
        public string ISSUER_CODE { get; set; }
        [XmlElement("ISIN")]
        public string ISIN { get; set; }
        [XmlElement("MAIN_TYPE")]
        public string MAIN_TYPE { get; set; }
        [XmlElement("SUB_TYPE")]
        public string SUB_TYPE { get; set; }
        [XmlElement("PRICE_HI")]
        public string PRICE_HI { get; set; }
        [XmlElement("PRICE_LOW")]
        public string PRICE_LOW { get; set; }
        [XmlElement("CLOSE_PRICE")]
        public string CLOSE_PRICE { get; set; }
        [XmlElement("AVERAGE_PRICE")]
        public string AVERAGE_PRICE { get; set; }
        [XmlElement("SHARE_VOLUME")]
        public string SHARE_VOLUME { get; set; }
        [XmlElement("num_of_trades")]
        public string num_of_trades { get; set; }
        [XmlElement("TURNOVER")]
        public string TURNOVER { get; set; }
        [XmlElement("trade_date")]
        public string trade_date { get; set; }
    }

    [XmlRoot("eSCROW_CLIENTSs")]
    public class eSCROW_CLIENTSs
    {
        [XmlElement("ESCROWCLIENTS")]
        public List<ESCROWCLIENTS> eSCROW_CLIENTSss { get; set; }

    }

    public class ESCROWCLIENTS
    {
        [XmlElement("MEMBER_CODE")]
        public String MEMBER_CODE { get; set; }
        [XmlElement("MEMBER_TYPE")]
        public String MEMBER_TYPE { get; set; }
        [XmlElement("CLIENT_PREFIX")]
        public String CLIENT_PREFIX { get; set; }
        [XmlElement("CLIENT_SUFFIX")]
        public String CLIENT_SUFFIX { get; set; }
        [XmlElement("CLIENT_TYPE")]
        public String CLIENT_TYPE { get; set; }
        [XmlElement("SURNAME")]
        public String SURNAME { get; set; }
        [XmlElement("OTHER_NAMES")]
        public String OTHER_NAMES { get; set; }
        [XmlElement("DATE_OF_REGISTRATION")]
        public String DATE_OF_REGISTRATION { get; set; }
        [XmlElement("DATE_CHANGED")]
        public String DATE_CHANGED { get; set; }
    }
    [XmlRoot("eSCROW_MINI_STMTs")]
    public class eSCROW_MINI_STMTs
    {
        [XmlElement("ESCROWMINISTMT")]
        public List<ESCROWMINISTMT> ESCROWMINISTMTs { get; set; }

    }

    public class ESCROWMINISTMT
    {
        [XmlElement("CLIENT_PREFIX")]
        public String CLIENT_PREFIX { get; set; }
        [XmlElement("MEMBER_CODE")]
        public String MEMBER_CODE { get; set; }
        [XmlElement("ISSUER_CODE")]
        public String ISSUER_CODE { get; set; }
        [XmlElement("MAIN_TYPE")]
        public String MAIN_TYPE { get; set; }
        [XmlElement("description")]
        public String description { get; set; }
        [XmlElement("date_system")]
        public String date_system { get; set; }
        [XmlElement("tshares")]
        public String tshares { get; set; }
    }
    public class ESCROWMINISTMTS
    {

        public String CLIENT_PREFIX { get; set; }
       
        public String MEMBER_CODE { get; set; }
       
        public String ISSUER_CODE { get; set; }
        
        public String MAIN_TYPE { get; set; }
    
        public String description { get; set; }
      
        public DateTime date_system { get; set; }

        public decimal tshares { get; set; }
        public decimal tsharess { get; set; }
    }
}