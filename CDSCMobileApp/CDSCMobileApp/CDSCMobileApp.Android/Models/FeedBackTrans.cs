﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;


namespace CDSCMobileApp.Models
{
  
    public class FeedBackTrans
    {
    
        public int FeedBackTransID { get; set; }
        public string ticketRef { get; set; }
        
        public int? UserID { get; set; }
        
        public string responsemessage { get; set; }
               public DateTime datereceived { get; set; }
        public Boolean IsClientNotification { get; set; }
        public Boolean IsAppNotification { get; set; }

        public int? FeedID { get; set; }
      //navigation property
      
    }
}