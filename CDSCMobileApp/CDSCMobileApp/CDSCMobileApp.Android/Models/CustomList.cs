using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics.Drawables;

using CDSCMobileApp.Models;
using CDSCMobileApp.Droid;
using Android.App;
using CDSCMobileApp.DROID;

namespace CDSCMobileApp.Model
{
    class CustomList : BaseAdapter
    {
        private Activity context;
        public List<Productss> listitem;

        private Drawable[] imageId;
        //public override int Count
        //{
        //    get
        //    {
        //        return listitem.Count;
        //    }
        //}
    
        public CustomList(Activity context, List<Productss> listitem,Drawable[] imageId)
        {
            this.context = context;
            this.listitem = listitem;
            
            this.imageId = imageId;
        }
        public override int Count
        {
            get { return listitem.Count; }
        }
        public override Java.Lang.Object GetItem(int position)
        {
            return position;
        }
        public override long GetItemId(int position)
        {
            return position;
        }

        public Productss GetItemAtPosition(int position)
        {
            return listitem[position];
        }
        public override View GetView(int position, View convertView, ViewGroup parent)
        {

            var item = listitem[position];
            var view = context.LayoutInflater.Inflate(Resource.Layout.list_single, parent, false);
            TextView txtTitle = (TextView)view.FindViewById(Resource.Id.txt);
            ImageView imageView = (ImageView)view.FindViewById(Resource.Id.img);
            txtTitle.SetText(item.ToString(), TextView.BufferType.Normal);
            //txtTitle.Text = (listitem[position]).ToString();
            //txtDate.Text = (listitem[position]).ToString();
            imageView.SetImageDrawable(imageId[position]);
            return view;
        }
    }
}