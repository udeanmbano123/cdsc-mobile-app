﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Widget;
using CDSCMobileApp.Droid;
using RestSharp;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;

namespace CDSCMobileApp.Activities
{
    [Activity(Label = "FORGOT PASSWORD", Theme = "@style/AppTheme2", MainLauncher = false, Icon = "@drawable/ctrademobile")]
    public class ForgotActivity : AppCompatActivity
    {
        //int count = 1;
        Button btn, btn2;
        EditText eml;
        public static string baseUrl { get; set; }
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Forgot);
            btn = FindViewById<Button>(Resource.Id.btnLogin);
            btn2 = FindViewById<Button>(Resource.Id.btnBack);
            eml = FindViewById<EditText>(Resource.Id.email);

               baseUrl = "http://197.248.8.104/CDSCW";
           
            btn2.Click += delegate
            {
                StartActivity(typeof(ActivityTradingAccount));
            };

            btn.Click += Btn_Click;
        }

        private void Btn_Click(object sender, EventArgs e)
        {
            try 
	{	        
		bool validA = eml.Text.All(c => Char.IsLetterOrDigit(c) || c.Equals('_'));

            if (eml.Text=="")
            {
                Toast.MakeText(this, "Client Number is required", ToastLength.Short).Show();
                return;
            }
           else
            {
              
                var client = new RestClient(baseUrl);
                var request = new RestRequest("Forg/{s}", Method.POST);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request.AddUrlSegment("s", eml.Text.ToString().Replace("*", "-"));
                IRestResponse response = client.Execute(request);
                string validate = response.Content;

                validate = validate.Replace(@"""", "");
                string val = validate;

               

                Toast.MakeText(this, val, ToastLength.Short).Show();
                eml.Text = "";
                return;
            }

	}
	catch (Exception f)
	{

                Toast.MakeText(this, "No internet or wifi", ToastLength.Short).Show();
                return;
            }

        }

        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = System.Text.Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);
           
            return BitConverter.ToString(hashedBytes);
        }
        public bool IsAddressAvailable(string address)
        {

            Boolean isHttpOk;
            try
            {
                Stream sStream;
                HttpWebRequest urlReq;
                HttpWebResponse urlRes;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(address);
                req.Method = "HEAD";
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                isHttpOk = resp.StatusCode == HttpStatusCode.OK;
                resp.Close();

                return isHttpOk;
            }
            catch (Exception)
            {
                isHttpOk = false;
                return isHttpOk;
            }
        }

    }
    }


