﻿using System;
using System.Collections.Generic;
using Android.Support.V7.Widget;
using Android.Views;
using CDSCMobileApp;
using Android.Widget;
using CDSCMobileApp.Droid;
using Android.Graphics.Drawables;
using System.Globalization;

namespace CDSCMobileeApp.DROID.Fragments
{

    public class CDAAdapter : RecyclerView.Adapter
    {
       // public dATASET_PRICESs mPhotoAlbum;
        public List<DATASETCDA> mPhotoAlbum;
        private Drawable[] imageId;
        public CDAAdapter(List<DATASETCDA> photoAlbum, Drawable[] imageId)
        {
            mPhotoAlbum = photoAlbum;
            this.imageId = imageId;
        }

        public override RecyclerView.ViewHolder
            OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).
                        Inflate(Resource.Layout.CDACardView, parent, false);
           CDAViewHolder vh = new CDAViewHolder(itemView);
            return vh;
        }

        public override void
            OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            CDAViewHolder vh = holder as CDAViewHolder;
           vh.Image.SetImageDrawable(imageId[position]);
           
            vh.Caption.Text = "NAME "+ mPhotoAlbum[position].NAME +"\nADDRESS :" +mPhotoAlbum[position].ADDRESS1+"\nTOWN :"+ mPhotoAlbum[position].TOWN;
            vh.Caption2.Text = "POST CODE " + mPhotoAlbum[position].POST_CODE + "\nCOUNTRY CODE " + mPhotoAlbum[position].COUNTRY_CODE + "\nFAX " + mPhotoAlbum[position].fax;
            vh.Caption3.Text = "TELEPHONE " + mPhotoAlbum[position].TELEPHONE;
        }

        public override int ItemCount
        {
            get { return mPhotoAlbum.Count; }
        }
    }
    public class CDAViewHolder : RecyclerView.ViewHolder
    {
        public ImageView Image { get; private set; }
        public TextView Caption { get; private set; }
        public TextView Caption2 { get; private set; }
        public TextView Caption3 { get; private set; }

        public CDAViewHolder(View itemView) : base(itemView)
        {
            // Locate and cache view references:
           Image = itemView.FindViewById<ImageView>(Resource.Id.imageView);
           Caption = itemView.FindViewById<TextView>(Resource.Id.textView);
           Caption2 = itemView.FindViewById<TextView>(Resource.Id.textView2);
          Caption3 = itemView.FindViewById<TextView>(Resource.Id.textView3);
        }
    }
}