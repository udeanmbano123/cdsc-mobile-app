﻿using System;
using System.Collections.Generic;
using Android.Support.V7.Widget;
using Android.Views;
using CDSCMobileApp;
using Android.Widget;
using CDSCMobileApp.Droid;
using Android.Graphics.Drawables;
using System.Globalization;
using CDSCMobileApp.DROID;

namespace CDSCMobileeApp.DROID.Fragments
{

    public class ProdAdapter : RecyclerView.Adapter
    {
       // public dATASET_PRICESs mPhotoAlbum;
        public List<Productss> mPhotoAlbum;
        private Drawable[] imageId;
        public ProdAdapter(List<Productss> photoAlbum, Drawable[] imageId)
        {
            mPhotoAlbum = photoAlbum;
            this.imageId = imageId;
        }

        public override RecyclerView.ViewHolder
            OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).
                        Inflate(Resource.Layout.ProdCardView, parent, false);
           ProdViewHolder vh = new ProdViewHolder(itemView);
            return vh;
        }

        public override void
            OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            ProdViewHolder vh = holder as ProdViewHolder;
           vh.Image.SetImageDrawable(imageId[position]);

            vh.Caption.Text = "NAME " + mPhotoAlbum[position].Name;
        }

        public override int ItemCount
        {
            get { return mPhotoAlbum.Count; }
        }
    }
    public class ProdViewHolder : RecyclerView.ViewHolder
    {
        public ImageView Image { get; private set; }
        public TextView Caption { get; private set; }
    

        public ProdViewHolder(View itemView) : base(itemView)
        {
            // Locate and cache view references:
           Image = itemView.FindViewById<ImageView>(Resource.Id.imageView);
           Caption = itemView.FindViewById<TextView>(Resource.Id.textView);
        }
    }
}