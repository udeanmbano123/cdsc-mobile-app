﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using CDSCMobileeApp.DROID.Fragments;
using System.Xml.Serialization;
using System.IO;
using Android.Graphics.Drawables;
using Android.Graphics;
using Android.Support.V4.App;
using RestSharp;

namespace CDSCMobileApp.Droid.Fragments
{
    public class Issuer : Fragment
    {
        RecyclerView newn;
        RecyclerView.LayoutManager mLayoutManager;
        IssueAdapter mAdapter;
        List<DATASETISSUERS> mPhotoAlbum;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }
        public static Issuer NewInstance()
        {
            var frag1 = new Issuer { Arguments = new Bundle() };
            return frag1;
        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            this.Activity.SetTitle(Resource.String.market);
Intent downloadIntent = new Intent(this.Activity, typeof(AppUsageIntent));
            UserActivity.title = "MARKET WATCH";
            // This is just one example of passing some values to an IntentService via the Intent:
            this.Activity.StartService(downloadIntent);

            View view = inflater.Inflate(Resource.Layout.ActivityPriceNotification, null);
            newn = view.FindViewById<RecyclerView>(Resource.Id.recyclerView);
            string validate = "";
            // Plug in the linear layout manager:
            try 
	{	        
		mLayoutManager = new LinearLayoutManager(this.Activity);
            newn.SetLayoutManager(mLayoutManager);
      	        
		 var client = new RestClient(UserActivity.baseurl);
            var request = new RestRequest("dtissue", Method.GET);
            IRestResponse response = client.Execute(request);
             validate= response.Content;

            if (validate.Contains("dATASET_ISSUERSs")==false)
            {
 //validate = @"<dATASET_ISSUERSs><DATASETISSUERS><AVERAGE_PRICE>0</AVERAGE_PRICE><COUNTRY_OF_INITIAL_ISSUE>KE</COUNTRY_OF_INITIAL_ISSUE><FH_LIMIT>100</FH_LIMIT><FOREIGN_HOLDING>310000000</FOREIGN_HOLDING><INDEXED_PRICE>0</INDEXED_PRICE><INDEXED_QUANTITY>760300000</INDEXED_QUANTITY><ISIN>KE4000003600</ISIN><ISSUER_CODE>SHEB</ISSUER_CODE><MAIN_TYPE>C</MAIN_TYPE><NAME>SHELTER AFRIQUE</NAME><PAR_VALUE>1</PAR_VALUE><PREVIOUS_CLOSE>0</PREVIOUS_CLOSE><PREVIOUS_INDEXED_PRICE>0</PREVIOUS_INDEXED_PRICE><PRICE_LAST_TRADED>0</PRICE_LAST_TRADED><QTY_ISSUED>760300000</QTY_ISSUED><qty_demmarted>760300000</qty_demmarted><REFERENCE_PRICE>100</REFERENCE_PRICE><SECURITY_CATEGORY>C</SECURITY_CATEGORY><SHORT_NAME>SHEAF FR/2018/5</SHORT_NAME><STATUS>1</STATUS><SUB_TYPE>0034</SUB_TYPE><status_description>Dealings Allowed</status_description></DATASETISSUERS><DATASETISSUERS><AVERAGE_PRICE>105</AVERAGE_PRICE><COUNTRY_OF_INITIAL_ISSUE>KE</COUNTRY_OF_INITIAL_ISSUE><FH_LIMIT>100</FH_LIMIT><FOREIGN_HOLDING>5000000</FOREIGN_HOLDING><INDEXED_PRICE>0</INDEXED_PRICE><INDEXED_QUANTITY>3429000000</INDEXED_QUANTITY><ISIN>KE4000003584</ISIN><ISSUER_CODE>I&amp;MB</ISSUER_CODE><MAIN_TYPE>C</MAIN_TYPE><NAME>INVESTMENTS &amp; MORTGAGES HOLDINGS LIMITED</NAME><PAR_VALUE>1</PAR_VALUE><PREVIOUS_CLOSE>105</PREVIOUS_CLOSE><PREVIOUS_INDEXED_PRICE>0</PREVIOUS_INDEXED_PRICE><PRICE_LAST_TRADED>105</PRICE_LAST_TRADED><QTY_ISSUED>3429000000</QTY_ISSUED><qty_demmarted>3429000000</qty_demmarted><REFERENCE_PRICE>100</REFERENCE_PRICE><SECURITY_CATEGORY>C</SECURITY_CATEGORY><SHORT_NAME>I&amp;M FXD/2019/5</SHORT_NAME><STATUS>1</STATUS><SUB_TYPE>0035</SUB_TYPE><status_description>Dealings Allowed</status_description></DATASETISSUERS><DATASETISSUERS><AVERAGE_PRICE>0</AVERAGE_PRICE><COUNTRY_OF_INITIAL_ISSUE>KE</COUNTRY_OF_INITIAL_ISSUE><FH_LIMIT>100</FH_LIMIT><INDEXED_PRICE>0</INDEXED_PRICE><INDEXED_QUANTITY>226000000</INDEXED_QUANTITY><ISIN>KE4000003709</ISIN><ISSUER_CODE>I&amp;MB</ISSUER_CODE><MAIN_TYPE>C</MAIN_TYPE><NAME>INVESTMENTS &amp; MORTGAGES HOLDINGS LIMITED</NAME><PAR_VALUE>1</PAR_VALUE><PREVIOUS_CLOSE>0</PREVIOUS_CLOSE><PREVIOUS_INDEXED_PRICE>0</PREVIOUS_INDEXED_PRICE><PRICE_LAST_TRADED>0</PRICE_LAST_TRADED><QTY_ISSUED>226000000</QTY_ISSUED><qty_demmarted>226000000</qty_demmarted><REFERENCE_PRICE>100</REFERENCE_PRICE><SECURITY_CATEGORY>C</SECURITY_CATEGORY><SHORT_NAME>I&amp;M FR/2019/5</SHORT_NAME><STATUS>1</STATUS><SUB_TYPE>0036</SUB_TYPE><status_description>Dealings Allowed</status_description></DATASETISSUERS><DATASETISSUERS><AVERAGE_PRICE>102</AVERAGE_PRICE><COUNTRY_OF_INITIAL_ISSUE>KE</COUNTRY_OF_INITIAL_ISSUE><FH_LIMIT>100</FH_LIMIT><FOREIGN_HOLDING>1750000</FOREIGN_HOLDING><INDEXED_PRICE>0</INDEXED_PRICE><INDEXED_QUANTITY>6000000000</INDEXED_QUANTITY><ISIN>KE3000009450</ISIN><ISSUER_CODE>BRTB</ISSUER_CODE><MAIN_TYPE>C</MAIN_TYPE><NAME>BRITISH-AMERICAN INVESTMENTS COMPANY (KENYA) LIMITED</NAME><PAR_VALUE>1</PAR_VALUE><PREVIOUS_CLOSE>102</PREVIOUS_CLOSE><PREVIOUS_INDEXED_PRICE>0</PREVIOUS_INDEXED_PRICE><PRICE_LAST_TRADED>102</PRICE_LAST_TRADED><QTY_ISSUED>6000000000</QTY_ISSUED><qty_demmarted>6000000000</qty_demmarted><REFERENCE_PRICE>100</REFERENCE_PRICE><SECURITY_CATEGORY>C</SECURITY_CATEGORY><SHORT_NAME>BRIT FXD/2019/5</SHORT_NAME><STATUS>1</STATUS><SUB_TYPE>0037</SUB_TYPE><status_description>Dealings Allowed</status_description></DATASETISSUERS><DATASETISSUERS><AVERAGE_PRICE>105</AVERAGE_PRICE><COUNTRY_OF_INITIAL_ISSUE>KE</COUNTRY_OF_INITIAL_ISSUE><FH_LIMIT>100</FH_LIMIT><FOREIGN_HOLDING>700000</FOREIGN_HOLDING><INDEXED_PRICE>0</INDEXED_PRICE><INDEXED_QUANTITY>2000000000</INDEXED_QUANTITY><ISIN>KE4000003816</ISIN><ISSUER_CODE>UAPB</ISSUER_CODE><MAIN_TYPE>C</MAIN_TYPE><NAME>UAP HOLDINGS LIMITED</NAME><PAR_VALUE>1</PAR_VALUE><PREVIOUS_CLOSE>105</PREVIOUS_CLOSE><PREVIOUS_INDEXED_PRICE>0</PREVIOUS_INDEXED_PRICE><PRICE_LAST_TRADED>105</PRICE_LAST_TRADED><QTY_ISSUED>2000000000</QTY_ISSUED><qty_demmarted>2000000000</qty_demmarted><REFERENCE_PRICE>100</REFERENCE_PRICE><SECURITY_CATEGORY>C</SECURITY_CATEGORY><SHORT_NAME>UAP FXD/2019/5</SHORT_NAME><STATUS>1</STATUS><SUB_TYPE>0038</SUB_TYPE><status_description>Dealings Allowed</status_description></DATASETISSUERS><DATASETISSUERS><AVERAGE_PRICE>21</AVERAGE_PRICE><COUNTRY_OF_INITIAL_ISSUE>KE</COUNTRY_OF_INITIAL_ISSUE><DATE_LAST_INDEXED>01-10-2015</DATE_LAST_INDEXED><FH_LIMIT>75</FH_LIMIT><FOREIGN_HOLDING>54308512</FOREIGN_HOLDING><INDEXED_PRICE>21</INDEXED_PRICE><INDEXED_QUANTITY>194625000</INDEXED_QUANTITY><ISIN>KE3000009674</ISIN><ISSUER_CODE>NSE</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><NAME>NIGERIA STOCK EXCHANGE</NAME><PAR_VALUE>4</PAR_VALUE><PREVIOUS_CLOSE>21</PREVIOUS_CLOSE><PREVIOUS_INDEXED_PRICE>21</PREVIOUS_INDEXED_PRICE><PRICE_LAST_TRADED>21</PRICE_LAST_TRADED><QTY_ISSUED>194625000</QTY_ISSUED><qty_demmarted>194625000</qty_demmarted><REFERENCE_PRICE>9.5</REFERENCE_PRICE><SECURITY_CATEGORY>E</SECURITY_CATEGORY><SHORT_NAME>NSE</SHORT_NAME><STATUS>1</STATUS><SUB_TYPE>0000</SUB_TYPE><status_description>Dealings Allowed</status_description></DATASETISSUERS><DATASETISSUERS><AVERAGE_PRICE>21</AVERAGE_PRICE><COUNTRY_OF_INITIAL_ISSUE>KE</COUNTRY_OF_INITIAL_ISSUE><DATE_LAST_INDEXED>01-10-2015</DATE_LAST_INDEXED><FH_LIMIT>75</FH_LIMIT><FOREIGN_HOLDING>54308512</FOREIGN_HOLDING><INDEXED_PRICE>21</INDEXED_PRICE><INDEXED_QUANTITY>194625000</INDEXED_QUANTITY><ISIN>KE3000009674</ISIN><ISSUER_CODE>NSE</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><NAME>NAIROBI SECURITIES EXCHANGE</NAME><PAR_VALUE>4</PAR_VALUE><PREVIOUS_CLOSE>21</PREVIOUS_CLOSE><PREVIOUS_INDEXED_PRICE>21</PREVIOUS_INDEXED_PRICE><PRICE_LAST_TRADED>21</PRICE_LAST_TRADED><QTY_ISSUED>194625000</QTY_ISSUED><qty_demmarted>194625000</qty_demmarted><REFERENCE_PRICE>9.5</REFERENCE_PRICE><SECURITY_CATEGORY>E</SECURITY_CATEGORY><SHORT_NAME>NSE</SHORT_NAME><STATUS>1</STATUS><SUB_TYPE>0000</SUB_TYPE><status_description>Dealings Allowed</status_description></DATASETISSUERS><DATASETISSUERS><AVERAGE_PRICE>99.5</AVERAGE_PRICE><COUNTRY_OF_INITIAL_ISSUE>KE</COUNTRY_OF_INITIAL_ISSUE><FH_LIMIT>75</FH_LIMIT><FOREIGN_HOLDING>13400000</FOREIGN_HOLDING><INDEXED_PRICE>0</INDEXED_PRICE><INDEXED_QUANTITY>5517600000</INDEXED_QUANTITY><ISIN>KE3000009898</ISIN><ISSUER_CODE>NICB</ISSUER_CODE><MAIN_TYPE>C</MAIN_TYPE><NAME>NATIONAL INDUSTRIAL CREDIT BANK LTD</NAME><PAR_VALUE>1</PAR_VALUE><PREVIOUS_CLOSE>99.5</PREVIOUS_CLOSE><PREVIOUS_INDEXED_PRICE>0</PREVIOUS_INDEXED_PRICE><PRICE_LAST_TRADED>99.5</PRICE_LAST_TRADED><QTY_ISSUED>5517600000</QTY_ISSUED><qty_demmarted>5517600000</qty_demmarted><REFERENCE_PRICE>100</REFERENCE_PRICE><SECURITY_CATEGORY>C</SECURITY_CATEGORY><SHORT_NAME>NIC FXD1/2014/5</SHORT_NAME><STATUS>1</STATUS><SUB_TYPE>0039</SUB_TYPE><status_description>Dealings Allowed</status_description></DATASETISSUERS><DATASETISSUERS><AVERAGE_PRICE>102</AVERAGE_PRICE><COUNTRY_OF_INITIAL_ISSUE>KE</COUNTRY_OF_INITIAL_ISSUE><FH_LIMIT>75</FH_LIMIT><FOREIGN_HOLDING>2500000</FOREIGN_HOLDING><INDEXED_PRICE>0</INDEXED_PRICE><INDEXED_QUANTITY>5000000000</INDEXED_QUANTITY><ISIN>KE3000009906</ISIN><ISSUER_CODE>CICB</ISSUER_CODE><MAIN_TYPE>C</MAIN_TYPE><NAME>CIC INSURANCE GROUP LTD</NAME><PAR_VALUE>1</PAR_VALUE><PREVIOUS_CLOSE>102</PREVIOUS_CLOSE><PREVIOUS_INDEXED_PRICE>0</PREVIOUS_INDEXED_PRICE><PRICE_LAST_TRADED>102</PRICE_LAST_TRADED><QTY_ISSUED>5000000000</QTY_ISSUED><qty_demmarted>5000000000</qty_demmarted><REFERENCE_PRICE>100</REFERENCE_PRICE><SECURITY_CATEGORY>C</SECURITY_CATEGORY><SHORT_NAME>CIC FXD1/2014/5</SHORT_NAME><STATUS>1</STATUS><SUB_TYPE>0040</SUB_TYPE><status_description>Dealings Allowed</status_description></DATASETISSUERS><DATASETISSUERS><AVERAGE_PRICE>103</AVERAGE_PRICE><COUNTRY_OF_INITIAL_ISSUE>KE</COUNTRY_OF_INITIAL_ISSUE><FH_LIMIT>75</FH_LIMIT><INDEXED_PRICE>0</INDEXED_PRICE><INDEXED_QUANTITY>4000000000</INDEXED_QUANTITY><ISIN>KE4000002438</ISIN><ISSUER_CODE>CFCB</ISSUER_CODE><MAIN_TYPE>C</MAIN_TYPE><NAME>CFC STANBIC BANK</NAME><PAR_VALUE>1</PAR_VALUE><PREVIOUS_CLOSE>103</PREVIOUS_CLOSE><PREVIOUS_INDEXED_PRICE>0</PREVIOUS_INDEXED_PRICE><PRICE_LAST_TRADED>103</PRICE_LAST_TRADED><QTY_ISSUED>4000000000</QTY_ISSUED><qty_demmarted>4000000000</qty_demmarted><REFERENCE_PRICE>100</REFERENCE_PRICE><SECURITY_CATEGORY>C</SECURITY_CATEGORY><SHORT_NAME>CSB-FXD01/2014/7</SHORT_NAME><STATUS>1</STATUS><SUB_TYPE>0042</SUB_TYPE><status_description>Dealings Allowed</status_description></DATASETISSUERS></dATASET_ISSUERSs>";

            }
		 
	

            
              
            
            
            //Console.WriteLine(validate);
            dATASET_ISSUERSs result = null;
            List<dATASET_ISSUERSs> nn = null;
            XmlSerializer serializer = new XmlSerializer(typeof(dATASET_ISSUERSs));
            using (TextReader reader = new StringReader(validate))
            {
                try
                {
                    result = (dATASET_ISSUERSs)serializer.Deserialize(reader);

                }
                catch (Exception)
                {


                }
            }


            Drawable[] imageId = new Drawable[result.DATASETCDAs.Count];
            int x = 0;
            TextDrawable.TextDrawable drawable;
            foreach (var d in result.DATASETCDAs)
            {
                try
                {
                    //TextDrawable.TextDrawable drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BuildRound("ABC", GetRandomColor(), GetRandomColor());
                    drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BeginConfig().FontSize(28).TextColor(Color.Black).Bold().EndConfig().BuildRoundRect(d.ISSUER_CODE, GetRandomColor(),2, GetRandomColor());
                    imageId[x] = drawable;
                }
                catch (Exception)
                {
                    continue;
                }
                x += 1;
            }
            mPhotoAlbum = result.DATASETCDAs;
            int my = mPhotoAlbum.Count;
            int my2 = imageId.Length;
            // Plug in my adapter:
            mAdapter = new IssueAdapter(mPhotoAlbum, imageId);
            newn.SetAdapter(mAdapter);
            
	}
	catch (Exception f)
	{

                Toast.MakeText(this.Activity, "No internet or wifi", ToastLength.Short).Show();

            }
            return view;
        }
        static Random rand = new Random();
        public static Color GetRandomColor()
        {
            int hue = rand.Next(255);
            Color color = Color.HSVToColor(
                new[] {
            hue,
            1.0f,
            1.0f,
                }
            );
            return color;
        }
    }
}