using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using CDSCMobileApp.Droid.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Cryptography;
using System.Text;

namespace CDSCMobileApp.Droid.Fragments
{
    public class ProfileUpdate : Fragment
    {
        EditText username, pass, pass2;
        Button btn, btn2;
        public static string baseurl { get; set; }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
           
                baseurl = "http://197.248.8.104/CDSCW";
           
            // Create your fragment here
        }

        public static ProfileUpdate NewInstance()
        {
            var frag1 = new ProfileUpdate{ Arguments = new Bundle() };
            return frag1;
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            View view = inflater.Inflate(Resource.Layout.ProfileUpdate, null);
            this.Activity.SetTitle(Resource.String.profile);

            username = (EditText)view.FindViewById(Resource.Id.username);
            pass = (EditText)view.FindViewById(Resource.Id.password);
            pass2 = (EditText)view.FindViewById(Resource.Id.confirmpassword);
            btn = (Button)view.FindViewById(Resource.Id.btnLogin);
            btn2 = (Button)view.FindViewById(Resource.Id.btnClear);
            btn.Click += Btn_Click;
            username.Text = UserActivity.member;
            username.Enabled = false;
            btn2.Click += delegate
            {

                pass2.Text = "";
                pass.Text = "";
            };
            var client = new RestClient(baseurl);
            var request = new RestRequest("ProfileAccept/{csdnumber}", Method.GET);
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("csdnumber", UserActivity.member);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
            List<Profile> dataList = JsonConvert.DeserializeObject<List<Profile>>(validate);
            foreach (var z in dataList)
            {
                pass.Text = z.Name;
                pass2.Text = z.Mail;
            }
            return view;
        }

        private void Btn_Click(object sender, System.EventArgs e)
        {
           if (pass.Text == "")
            {
                Toast.MakeText(this.Activity, "Username is required", ToastLength.Short).Show();
                return;

            }
            else if (pass2.Text == "")
            {
                Toast.MakeText(this.Activity, "Email is required", ToastLength.Short).Show();
                return;

            }
           else
            {
                btn.Enabled = false;
                var client = new RestClient(baseurl);
                var request = new RestRequest("Profile/{username}/{email}/{csdnumber}", Method.POST);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request.AddUrlSegment("username", pass.Text.Replace(" ", ""));
                request.AddUrlSegment("email",pass2.Text.Replace(" ", ""));
                request.AddUrlSegment("csdnumber", UserActivity.member);
                IRestResponse response = client.Execute(request);
              
                string validate = response.Content;

                validate = validate.Replace(@"""", "");
                string val = validate;
                Toast.MakeText(this.Activity, "Profile updated successfully", ToastLength.Short).Show();
                FragmentManager fm = this.FragmentManager;
                var trans = fm.BeginTransaction();
                trans.Replace(Resource.Id.content_frame, Fragment1.NewInstance(), "Fragment1");
                trans.AddToBackStack("ProfileUpdate");
                trans.Commit();
                return;
            }
        }
        public bool IsAddressAvailable(string address)
        {
            try
            {
                System.Net.WebClient client = new WebClient();
                client.DownloadData(address);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }
    }
}