﻿using System;
using System.Collections.Generic;
using Android.Support.V7.Widget;
using Android.Views;
using CDSCMobileApp;
using Android.Widget;
using CDSCMobileApp.Droid;
using Android.Graphics.Drawables;

namespace CDSCMobileeApp.DROID.Fragments
{

    public class IssueAdapter : RecyclerView.Adapter
    {
       // public dATASET_PRICESs mPhotoAlbum;
        public List<DATASETISSUERS> mPhotoAlbum;
        private Drawable[] imageId;
        public IssueAdapter(List<DATASETISSUERS> photoAlbum, Drawable[] imageId)
        {
            mPhotoAlbum = photoAlbum;
            this.imageId = imageId;
        }

        public override RecyclerView.ViewHolder
            OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView2 = LayoutInflater.From(parent.Context).
                        Inflate(Resource.Layout.IssueCardView, parent, false);
            IssuerViewHolder vh = new IssuerViewHolder(itemView2);
            return vh;
        }

        public override void
            OnBindViewHolder(RecyclerView.ViewHolder holder2, int position)
        {
            IssuerViewHolder vh = holder2 as IssuerViewHolder;
            vh.Image.SetImageDrawable(imageId[position]);
            vh.Image.Visibility = ViewStates.Invisible;
            vh.Caption2.Text =mPhotoAlbum[position].NAME;
            vh.Caption.Text= "KES" + mPhotoAlbum[position].INDEXED_PRICE + "\n" + mPhotoAlbum[position].INDEXED_QUANTITY+"\n"
                + "INDEXED:" + mPhotoAlbum[position].DATE_LAST_INDEXED + "\nLAST TRADED :" + mPhotoAlbum[position].DATE_LAST_TRADED;
            vh.Caption3.Text = "";
        }

        public override int ItemCount
        {
            get { return mPhotoAlbum.Count; }
        }
    };
    public class IssuerViewHolder : RecyclerView.ViewHolder
    {
        public ImageView Image { get; private set; }
        public TextView Caption { get; private set; }
        public TextView Caption2 { get; private set; }
        public TextView Caption3 { get; private set; }

        public IssuerViewHolder(View itemView2) : base(itemView2)
        {
            // Locate and cache view references:
           Image = itemView2.FindViewById<ImageView>(Resource.Id.imageView);
           Caption = itemView2.FindViewById<TextView>(Resource.Id.textView);
           Caption2 = itemView2.FindViewById<TextView>(Resource.Id.textView2);
          Caption3 = itemView2.FindViewById<TextView>(Resource.Id.textView3);
        }
    }
}