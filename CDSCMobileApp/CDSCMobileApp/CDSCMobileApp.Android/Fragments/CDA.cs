﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using CDSCMobileeApp.DROID.Fragments;
using System.Xml.Serialization;
using System.IO;
using Android.Graphics.Drawables;
using Android.Graphics;
using Android.Support.V4.App;
using RestSharp;

namespace CDSCMobileApp.Droid.Fragments
{
    public class CDA : Fragment
    {
        RecyclerView newn;
        RecyclerView.LayoutManager mLayoutManager;
        CDAAdapter mAdapter;
        List<DATASETCDA> mPhotoAlbum;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }
        public static CDA NewInstance()
        {
            var frag1 = new CDA { Arguments = new Bundle() };
            return frag1;
        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            this.Activity.SetTitle(Resource.String.deactivate);
            Intent downloadIntent = new Intent(this.Activity, typeof(AppUsageIntent));
            UserActivity.title = "CDAs";
            // This is just one example of passing some values to an IntentService via the Intent:
            this.Activity.StartService(downloadIntent);

            View view = inflater.Inflate(Resource.Layout.ActivityPriceNotification, null);
            newn= view.FindViewById<RecyclerView>(Resource.Id.recyclerView);
try 
	{	        
		
            // Plug in the linear layout manager:
            mLayoutManager = new LinearLayoutManager(this.Activity);
            newn.SetLayoutManager(mLayoutManager);
            var client = new RestClient(UserActivity.baseurl);
            var request = new RestRequest("dtcda", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            if (validate.Contains("dATASET_CDAs") == false)
            {
                //validate = "<dATASET_CDAs><DATASETCDA><ADDRESS1>P.O. BOX 30437</ADDRESS1><COUNTRY_CODE>KE</COUNTRY_CODE><fax>254 20 2734635</fax><MEMBER_CODE>B25</MEMBER_CODE><MEMBER_TYPE>B</MEMBER_TYPE><NAME>CBA CAPITAL LIMITED</NAME><POST_CODE>00100</POST_CODE><STATUS>1</STATUS><status_description>Dealings Allowed</status_description><TELEPHONE>254 20 2884000</TELEPHONE><TOWN>NAIROBI</TOWN></DATASETCDA><DATASETCDA><ADDRESS1>PO BOX 9959</ADDRESS1><COUNTRY_CODE>KE</COUNTRY_CODE><MEMBER_CODE>B43</MEMBER_CODE><MEMBER_TYPE>B</MEMBER_TYPE><NAME>GENGHIS CAPITAL LIMITED</NAME><STATUS>1</STATUS><status_description>Dealings Allowed</status_description><TELEPHONE>2774767/0/1/2</TELEPHONE></DATASETCDA><DATASETCDA><ADDRESS1>P.O.BOX 30389 - 00100</ADDRESS1><COUNTRY_CODE>KE</COUNTRY_CODE><e_MAIL>info@apalife.co.ke</e_MAIL><fax>3641100</fax><MEMBER_CODE>APAL</MEMBER_CODE><MEMBER_TYPE>C</MEMBER_TYPE><NAME>APA LIFE ASSURANCE LTD</NAME><STATUS>1</STATUS><status_description>Dealings Allowed</status_description><TELEPHONE>3641000</TELEPHONE><TOWN>NAIROBI</TOWN></DATASETCDA><DATASETCDA><ADDRESS1>P.O.BOX 30065 - 00100</ADDRESS1><COUNTRY_CODE>KE</COUNTRY_CODE><e_MAIL>info@apainsurance.org</e_MAIL><fax>2862200</fax><MEMBER_CODE>APAI</MEMBER_CODE><MEMBER_TYPE>C</MEMBER_TYPE><NAME>APA INSURANCE LIMITED</NAME><STATUS>1</STATUS><status_description>Dealings Allowed</status_description><TELEPHONE>2862000</TELEPHONE><TOWN>NAIROBI</TOWN></DATASETCDA><DATASETCDA><ADDRESS1>P.O BOX 74454</ADDRESS1><ADDRESS2>EQUITY CENTRE, 4TH FLOOR</ADDRESS2><ADDRESS3>HOSPITAL ROAD, UPPERHILL</ADDRESS3><COUNTRY_CODE>KE</COUNTRY_CODE><e_MAIL>info@equityinvestmentbank.co.k</e_MAIL><fax>020 8055798</fax><MEMBER_CODE>B26</MEMBER_CODE><MEMBER_TYPE>B</MEMBER_TYPE><NAME>EQUITY INVESTMENT BANK</NAME><POST_CODE>00200</POST_CODE><STATUS>1</STATUS><status_description>Dealings Allowed</status_description><TELEPHONE>0719056500-15</TELEPHONE><TOWN>NAIROBI</TOWN></DATASETCDA><DATASETCDA><ADDRESS1>P.O. BOX 45465</ADDRESS1><ADDRESS2>NAIROBI</ADDRESS2><ADDRESS3>HUGHES BUILDING  3RD FLOOR</ADDRESS3><COUNTRY_CODE>KE</COUNTRY_CODE><e_MAIL>info@francisdrummond.com</e_MAIL><fax>223061</fax><MEMBER_CODE>B01</MEMBER_CODE><MEMBER_TYPE>B</MEMBER_TYPE><NAME>DRUMMOND INVESTMENT BANK LTD.</NAME><STATUS>1</STATUS><status_description>Dealings Allowed</status_description><TELEPHONE>318689/90</TELEPHONE></DATASETCDA><DATASETCDA><ADDRESS1>P.O. BOX 45396</ADDRESS1><ADDRESS2>NAIROBI</ADDRESS2><COUNTRY_CODE>KE</COUNTRY_CODE><e_MAIL>admin@dyer.africaonline.co.ke</e_MAIL><fax>223061</fax><MEMBER_CODE>B02</MEMBER_CODE><MEMBER_TYPE>B</MEMBER_TYPE><NAME>DYER &amp; BLAIR INVESTMENT BANK LTD</NAME><STATUS>1</STATUS><status_description>Dealings Allowed</status_description><TELEPHONE>227803</TELEPHONE></DATASETCDA><DATASETCDA><ADDRESS1>P.O. BOX 46524</ADDRESS1><ADDRESS3>INTERNATIONAL HOUSE 13TH FLOOR</ADDRESS3><COUNTRY_CODE>KE</COUNTRY_CODE><e_MAIL>ftbrokers@net2000ke.com</e_MAIL><fax>228498</fax><MEMBER_CODE>B03</MEMBER_CODE><MEMBER_TYPE>B</MEMBER_TYPE><NAME>FRANCIS THUO &amp; PARTNERS LTD.</NAME><STATUS>3</STATUS><status_description>Trade Suspended</status_description><TELEPHONE>226531</TELEPHONE><TOWN>NAIROBI</TOWN></DATASETCDA><DATASETCDA><ADDRESS1>P.O. BOX 41868</ADDRESS1><ADDRESS2>NAIROBI</ADDRESS2><ADDRESS3>NATION CENTRE 12TH FLOOR</ADDRESS3><COUNTRY_CODE>KE</COUNTRY_CODE><e_MAIL>nyagastk@users.africaonline.co</e_MAIL><fax>332785</fax><MEMBER_CODE>B05</MEMBER_CODE><MEMBER_TYPE>B</MEMBER_TYPE><NAME>NYAGA STOCKBROKERS LTD.</NAME><STATUS>3</STATUS><status_description>Trade Suspended</status_description><TELEPHONE>332783</TELEPHONE></DATASETCDA><DATASETCDA><ADDRESS1>P.O. BOX 12185</ADDRESS1><ADDRESS2>NAIROBI</ADDRESS2><ADDRESS3>TRAVEL (UTC) HOUSE 5TH FLOOR</ADDRESS3><COUNTRY_CODE>KE</COUNTRY_CODE><e_MAIL>ngenye@insightkenya.com</e_MAIL><fax>217199</fax><MEMBER_CODE>B06</MEMBER_CODE><MEMBER_TYPE>B</MEMBER_TYPE><NAME>NGENYE KARIUKI CO. LTD.</NAME><STATUS>3</STATUS><status_description>Trade Suspended</status_description><TELEPHONE>224333</TELEPHONE></DATASETCDA></dATASET_CDAs>";
            }
            //Console.WriteLine(validate);
            dATASET_CDAs result = null;
            List<dATASET_CDAs> nn = null;
            XmlSerializer serializer = new XmlSerializer(typeof(dATASET_CDAs));
            using (TextReader reader = new StringReader(validate))
            {
                try
                {
                    result = (dATASET_CDAs)serializer.Deserialize(reader);

                }
                catch (Exception)
                {


                }
            }

            Drawable[] imageId = new Drawable[result.DATASETCDAs.Count];
            int x = 0;
            TextDrawable.TextDrawable drawable;
            foreach (var d in result.DATASETCDAs)
            {

                try
                {
                    //TextDrawable.TextDrawable drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BuildRound("ABC", GetRandomColor(), GetRandomColor());
                    drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BeginConfig().FontSize(28).TextColor(Color.Black).Bold().EndConfig().BuildRound(d.MEMBER_CODE, GetRandomColor(), GetRandomColor());
                    imageId[x] = drawable;
                }
                catch (Exception)
                {
                    continue;
                }
                x += 1;
            }
            mPhotoAlbum = result.DATASETCDAs;
            int my = mPhotoAlbum.Count;
            // Plug in my adapter:
            mAdapter = new CDAAdapter(mPhotoAlbum, imageId);
            newn.SetAdapter(mAdapter);

	}
	catch (Exception f)
	{

                Toast.MakeText(this.Activity, "No internet or wifi", ToastLength.Short).Show();

            }
            return view;
        }
        static Random rand = new Random();
        public static Color GetRandomColor()
        {
            int hue = rand.Next(255);
            Color color = Color.HSVToColor(
                new[] {
            hue,
            1.0f,
            1.0f,
                }
            );
            return color;
        }
    }
}