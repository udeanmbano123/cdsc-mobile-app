﻿using System;
using System.Collections.Generic;
using Android.Support.V7.Widget;
using Android.Views;
using CDSCMobileApp;
using Android.Widget;
using CDSCMobileApp.Droid;
using Android.Graphics.Drawables;
using System.Globalization;

namespace CDSCMobileeApp.DROID.Fragments
{

    public class PriceAdapter : RecyclerView.Adapter
    {
       // public dATASET_PRICESs mPhotoAlbum;
        public List<DATASETPRICES> mPhotoAlbum;
        private Drawable[] imageId;
        public PriceAdapter(List<DATASETPRICES> photoAlbum, Drawable[] imageId)
        {
            mPhotoAlbum = photoAlbum;
            this.imageId = imageId;
        }

        public override RecyclerView.ViewHolder
            OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).
                        Inflate(Resource.Layout.PriceCardView, parent, false);
            PriceViewHolder vh = new PriceViewHolder(itemView);
            return vh;
        }

        public override void
            OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            PriceViewHolder vh = holder as PriceViewHolder;
            vh.Image.SetImageDrawable(imageId[position]);
            float PRICE_HI = 0;
            float CLOSE_PRICE = 0;
            PRICE_HI= float.Parse(mPhotoAlbum[position].PRICE_HI, CultureInfo.InvariantCulture.NumberFormat);
            CLOSE_PRICE = float.Parse(mPhotoAlbum[position].CLOSE_PRICE, CultureInfo.InvariantCulture.NumberFormat);
            float change = 0;
            try
            {
                change = ((PRICE_HI-CLOSE_PRICE)/CLOSE_PRICE) * 100;
            }
            catch (Exception)
            {

                change = 0;
            }
            vh.Caption.Text = Convert.ToDateTime(mPhotoAlbum[position].trade_date).ToString("dd-MMM-yyyy") +"\n" + mPhotoAlbum[position].CLOSE_PRICE;
            vh.Caption2.Text = mPhotoAlbum[position].ISSUER_CODE+"\n"+mPhotoAlbum[position].ISIN;
            vh.Caption3.Text = mPhotoAlbum[position].CLOSE_PRICE+" ("+ string.Format("{0:N4}", change) + "%)";
        }

        public override int ItemCount
        {
            get { return mPhotoAlbum.Count; }
        }
    }
    public class PriceViewHolder : RecyclerView.ViewHolder
    {
        public ImageView Image { get; private set; }
        public TextView Caption { get; private set; }
        public TextView Caption2 { get; private set; }
        public TextView Caption3 { get; private set; }

        public PriceViewHolder(View itemView) : base(itemView)
        {
            // Locate and cache view references:
           Image = itemView.FindViewById<ImageView>(Resource.Id.imageView);
           Caption = itemView.FindViewById<TextView>(Resource.Id.textView);
           Caption2 = itemView.FindViewById<TextView>(Resource.Id.textView2);
          Caption3 = itemView.FindViewById<TextView>(Resource.Id.textView3);
        }
    }
}