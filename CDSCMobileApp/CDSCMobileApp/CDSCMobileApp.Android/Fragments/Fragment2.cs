using Android.OS;


using Android.Webkit;
using Android.Widget;
using Android.Support.V4.App;
using Android.Views;

using RestSharp.Extensions.MonoHttp;
using Android.Content;

namespace CDSCMobileApp.Droid.Fragments
{
    public class Fragment2 : Fragment
    {
        WebView browser;
        Button btn;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public static Fragment2 NewInstance()
        {
            var frag2 = new Fragment2 { Arguments = new Bundle() };
            return frag2;
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
          View view=inflater.Inflate(Resource.Layout.previewmessage, null);
              btn = view.FindViewById<Button>(Resource.Id.wbbutton);
            WebView localWebView = view.FindViewById<WebView>(Resource.Id.LocalWebView);
            string name = UserActivity.datereceived;
            string response = UserActivity.responsemessage;
            UserActivity.title = "TICKET VIEWOPTION";
            this.Activity.SetTitle(Resource.String.deactivate);
            Intent downloadIntent = new Intent(this.Activity, typeof(AppUsageIntent));

            // This is just one example of passing some values to an IntentService via the Intent:
            this.Activity.StartService(downloadIntent);

            //localWebView.LoadData(htmlSource, "text/html", "UTF-8");
            localWebView.Settings.JavaScriptEnabled = true;
            localWebView.LoadUrl("file:///android_asset/Web/index.html?date="+ HttpUtility.UrlEncode(name).Replace("+", "%20") + "&response="+ HttpUtility.UrlEncode(response).Replace("+", "%20") + "");
            
            btn.Click += Btn_Click;
            return view;
        }

        private void Btn_Click(object sender, System.EventArgs e)
        {
            FragmentManager fm = this.FragmentManager;
            //fm.PopBackStack(0,0);
            var trans = fm.BeginTransaction();
            fm.PopBackStack();
        }
    }
}