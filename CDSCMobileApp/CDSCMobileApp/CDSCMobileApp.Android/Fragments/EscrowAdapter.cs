﻿using System;
using System.Collections.Generic;
using Android.Support.V7.Widget;
using Android.Views;
using CDSCMobileApp;
using Android.Widget;
using CDSCMobileApp.Droid;
using Android.Graphics.Drawables;
using System.Globalization;

namespace CDSCMobileeApp.DROID.Fragments
{

    public class EscrowAdapter : RecyclerView.Adapter
    {
       // public dATASET_PRICESs mPhotoAlbum;
        public List<ESCROWCLIENTS> mPhotoAlbum;
        private Drawable[] imageId;
        public EscrowAdapter(List<ESCROWCLIENTS> photoAlbum, Drawable[] imageId)
        {
            mPhotoAlbum = photoAlbum;
            this.imageId = imageId;
        }

        public override RecyclerView.ViewHolder
            OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).
                        Inflate(Resource.Layout.EscrowCardView, parent, false);
           EscrowViewHolder vh = new EscrowViewHolder(itemView);
            return vh;
        }

        public override void
            OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            EscrowViewHolder vh = holder as EscrowViewHolder;
           vh.Image.SetImageDrawable(imageId[position]);
           
            vh.Caption.Text = "CSD/ATP "+ mPhotoAlbum[position].CLIENT_PREFIX +"\nNAME :" +mPhotoAlbum[position].OTHER_NAMES+"\nSURNAME :"+ mPhotoAlbum[position].SURNAME;
            vh.Caption2.Text = "DATE REGISTRATION :" + Convert.ToDateTime(mPhotoAlbum[position].DATE_OF_REGISTRATION).ToString("dd-MMM-yyyy") + "\nDATE CHANGED " + Convert.ToDateTime(mPhotoAlbum[position].DATE_CHANGED).ToString("dd-MMM-yyyy") + "\nCLIENT TYPE :" + mPhotoAlbum[position].CLIENT_TYPE;
            vh.Caption3.Text = "MEMBER CODE :" + mPhotoAlbum[position].MEMBER_CODE + "\nMEMBER TYPE :"+ mPhotoAlbum[position].MEMBER_TYPE;
        }

        public override int ItemCount
        {
            get { return mPhotoAlbum.Count; }
        }
    }
    public class EscrowViewHolder : RecyclerView.ViewHolder
    {
        public ImageView Image { get; private set; }
        public TextView Caption { get; private set; }
        public TextView Caption2 { get; private set; }
        public TextView Caption3 { get; private set; }

        public EscrowViewHolder(View itemView) : base(itemView)
        {
            // Locate and cache view references:
           Image = itemView.FindViewById<ImageView>(Resource.Id.imageView);
           Caption = itemView.FindViewById<TextView>(Resource.Id.textView);
           Caption2 = itemView.FindViewById<TextView>(Resource.Id.textView2);
          Caption3 = itemView.FindViewById<TextView>(Resource.Id.textView3);
        }
    }
}