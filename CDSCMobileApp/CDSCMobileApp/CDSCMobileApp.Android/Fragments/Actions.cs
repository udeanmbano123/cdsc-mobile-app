using Android;

using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Views;
using Android.Widget;

using System;
using System.Collections.Generic;
using TextDrawable;
using System.Linq;
using RestSharp;
using Newtonsoft.Json;

using Android.Support.V4.App;
using CDSCMobileApp.Models;
using Android.Content;

namespace CDSCMobileApp.Droid.Fragments
{
    public class Actions: Android.Support.V4.App.Fragment
    {
       
        private List<Messages> mItems;
        private List<string> mItems2;
        private ListView mListView;
        Button btn,btn2;
        RadioButton rd1;
        RadioButton rd2;
        EditText tick;
        RadioGroup rad;
        Random random = new Random();
        CustomList adapter;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
           
            // Create your fragment here
        }
        // Xamarin.Android (using Android.Graphics;)
        static Random rand = new Random();
        public static Color GetRandomColor()
        {
            int hue = rand.Next(255);
            Color color = Color.HSVToColor(
                new[] {
            hue,
            1.0f,
            1.0f,
                }
            );
            return color;
        }
        public static Actions NewInstance()
        {
            var frag2 = new Actions { Arguments = new Bundle() };
            return frag2;
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            Intent downloadIntent = new Intent(this.Activity, typeof(AppUsageIntent));
            this.Activity.SetTitle(Resource.String.cuscall);
            UserActivity.title = "CUSTOMER CALL CENTRE";
            // This is just one example of passing some values to an IntentService via the Intent:
            this.Activity.StartService(downloadIntent);
            View view= inflater.Inflate(Resource.Layout.Actions, null);
            btn = view.FindViewById<Button>(Resource.Id.button1);
            btn2 = view.FindViewById<Button>(Resource.Id.button2);
            rd1 = view.FindViewById<RadioButton>(Resource.Id.radioButton1);
            rd2 = view.FindViewById<RadioButton>(Resource.Id.radioButton2);
             tick = view.FindViewById<EditText>(Resource.Id.ticketref);
            mListView = view.FindViewById<ListView>(Resource.Id.listView1);
             rad = view.FindViewById<RadioGroup>(Resource.Id.radioGroup1);
            mItems = new List<Messages>();
            mItems2 = new List<string>();

            rd1.Selected = true;
            mListView.ItemLongClick += MListView_ItemLongClick;

            btn.Click += Btn_Click;
            btn2.Click += Btn2_Click;
            rd1.Click += Rd1_Click;
            rd2.Click += Rd2_Click;
            return view;
        }

        private void Btn2_Click(object sender, EventArgs e)
        {
            FragmentManager fm = this.FragmentManager;
            var trans = fm.BeginTransaction();
            trans.Replace(Resource.Id.content_frame, Fragment5.NewInstance(), "Fragment2");
            trans.AddToBackStack(null);
            trans.Commit();
        }

        private void Rd2_Click(object sender, EventArgs e)
        {
            rd1.Selected = false;
            rd2.Selected = true;
        }

        private void Rd1_Click(object sender, EventArgs e)
        {
            rd2.Selected = false;
            rd1.Selected = true;
        }

        // Xamarin.Android (using Android.Graphics;)


        private void Btn_Click(object sender, EventArgs e)
        {
            string type = "";



            if (rd1.Selected==true)
            {
                type = "Inbox";
            }
            if (rd2.Selected==true)
            {
                type = "Sent";
            }
            if (tick.Text=="")
            {
                Toast.MakeText(this.Activity, "Ticket Reference is required", ToastLength.Long).Show();

                return;
            }else if (type=="")
            {
                Toast.MakeText(this.Activity, "Message type is required", ToastLength.Long).Show();

                return;
            }
            else
            {
                if (type == "Inbox")
                {
                    Toast.MakeText(this.Activity, "Searching please be patient..............Press hold message for full view", ToastLength.Long).Show();

                    try
                    {

                        mItems.Clear();
                        mItems2.Clear();
                        string tkt = tick.Text;
                        var client = new RestClient(UserActivity.baseurl);
                        var request = new RestRequest("ticketN/{ticket}/{type}", Method.GET);
                        request.AddUrlSegment("ticket", tkt.Replace(" ", string.Empty));
                        request.AddUrlSegment("type", type);
                        IRestResponse response = client.Execute(request);




                        string validate = response.Content;
                        List<FeedBackTrans> dataList = JsonConvert.DeserializeObject<List<FeedBackTrans>>(validate);

                        var dbsel = from s in dataList
                                    select s;

                        List<string> mylogs = new List<string>();

                        foreach (var z in dbsel)
                        {

                            mItems.Add(new Messages() { responsemessage = z.responsemessage, datereceived = Convert.ToDateTime(z.datereceived).ToString("yyyy-MM-dd") });
                        }

                        Drawable[] imageId = new Drawable[mItems.Count];

                        for (int x = 0; x < mItems.Count; x++)
                        {

                            //TextDrawable.TextDrawable drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BuildRound("ABC", GetRandomColor(), GetRandomColor());
                            TextDrawable.TextDrawable drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BeginConfig().FontSize(28).TextColor(Color.Black).Bold().EndConfig().BuildRound("CDSC", GetRandomColor(), GetRandomColor());
                            imageId[x] = drawable;
                        }

                        //ArrayAdapter<string> adapter = new ArrayAdapter<string>(this.Activity, Android.Resource.Layout.SimpleListItem1, objects: mItems.ToArray());
                        adapter = new CustomList(this.Activity, mItems, imageId);
                        mListView.Adapter = adapter;
                        //mListView.ItemClick += MListView_ItemClick;

                    }
                    catch (Exception)
                    {

                        
                    }

                }
                else
                {
                    Toast.MakeText(this.Activity, "Searching please be patient..............Press hold message for full view", ToastLength.Long).Show();
                    try
                    {

                        mItems.Clear();
                        mItems2.Clear();
                        string tkt = tick.Text;
                        var client = new RestClient(UserActivity.baseurl);
                        var request = new RestRequest("ticketN/{ticket}/{type}", Method.GET);
                        request.AddUrlSegment("ticket", tkt.Replace(" ", string.Empty));
                        request.AddUrlSegment("type", type);
                        IRestResponse response = client.Execute(request);



                        string validate = response.Content;
                        List<FeedBackTrans> dataList = JsonConvert.DeserializeObject<List<FeedBackTrans>>(validate);

                        var dbsel = from s in dataList
                                    select s;

                        List<string> mylogs = new List<string>();

                        foreach (var z in dbsel)
                        {

                            mItems.Add(new Messages() { responsemessage = z.responsemessage, datereceived = Convert.ToDateTime(z.datereceived).ToString("yyyy-MM-dd") });
                        }

                        Drawable[] imageId = new Drawable[mItems.Count];

                        for (int x = 0; x < mItems.Count; x++)
                        {

                            //TextDrawable.TextDrawable drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BuildRound("ABC", GetRandomColor(), GetRandomColor());
                            TextDrawable.TextDrawable drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BeginConfig().FontSize(28).TextColor(Color.Black).Bold().EndConfig().BuildRound("USR", GetRandomColor(), GetRandomColor());
                            imageId[x] = drawable;
                        }

                        //ArrayAdapter<string> adapter = new ArrayAdapter<string>(this.Activity, Android.Resource.Layout.SimpleListItem1, objects: mItems.ToArray());
                        adapter = new CustomList(this.Activity, mItems, imageId);
                        mListView.Adapter = adapter;
                        //mListView.ItemClick += MListView_ItemClick;


                    }
                    catch (Exception)
                    {

                        
                    }

                }

            }

           
        }

        private void MListView_ItemLongClick(object sender, AdapterView.ItemLongClickEventArgs e)
        {



            var item = adapter.GetItemAtPosition(e.Position);
            //Make a toast with the item name just to show it was clicked
            Toast.MakeText(this.Activity, item.responsemessage + " Clicked!", ToastLength.Short).Show();
            UserActivity.responsemessage = item.responsemessage;
            UserActivity.datereceived = item.datereceived;
            FragmentManager fm = this.FragmentManager;
            //fm.PopBackStack(0,0);
            var trans = fm.BeginTransaction();
            trans.Replace(Resource.Id.content_frame, Fragment2.NewInstance(), "Fragment2");
            trans.AddToBackStack(null);
            trans.Commit();
        }


    }
}