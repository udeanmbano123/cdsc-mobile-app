﻿using System;
using System.Collections.Generic;
using Android.Support.V7.Widget;
using Android.Views;
using CDSCMobileApp;
using Android.Widget;
using CDSCMobileApp.Droid;
using Android.Graphics.Drawables;

namespace CDSCMobileeApp.DROID.Fragments
{

    public class StatementAdapter : RecyclerView.Adapter
    {
       // public dATASET_PRICESs mPhotoAlbum;
        public List<ESCROWMINISTMTS> mPhotoAlbum;
        public Drawable[] imageId;
        public decimal Balance;
     
        public StatementAdapter(List<ESCROWMINISTMTS> photoAlbum, Drawable[] imageId,decimal Balance)
        {
            mPhotoAlbum = photoAlbum;
            this.imageId = imageId;
            this.Balance = Balance;
        }

        public override RecyclerView.ViewHolder
            OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).
                        Inflate(Resource.Layout.StatementCardView, parent, false);
            StmtViewHolder vh = new StmtViewHolder(itemView);
            return vh;
        }

        public override void
            OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            StmtViewHolder vh = holder as StmtViewHolder;
            vh.Image.SetImageDrawable(imageId[position]);
            vh.Caption.Text = "TRANSACTION DATE :" + mPhotoAlbum[position].date_system.ToString("dd-MMM-yyyy");
            vh.Caption2.Text= "DESCRIPTION :"+mPhotoAlbum[position].description;
            vh.Caption3.Text ="AMOUNT KES " + mPhotoAlbum[position].tshares+"\nRUNNING BALANCE KES"+ mPhotoAlbum[position].tsharess;
        }

        public override int ItemCount
        {
            get { return mPhotoAlbum.Count; }
        }
    }
    public class StmtViewHolder : RecyclerView.ViewHolder
    {
        public ImageView Image { get; set; }
        public TextView Caption { get;  set; }
        public TextView Caption2 { get; set; }
        public TextView Caption3 { get;  set; }

        public StmtViewHolder(View itemView) : base(itemView)
        {
            // Locate and cache view references:
           Image = itemView.FindViewById<ImageView>(Resource.Id.imageView);
           Caption = itemView.FindViewById<TextView>(Resource.Id.textView);
           Caption2 = itemView.FindViewById<TextView>(Resource.Id.textView2);
          Caption3 = itemView.FindViewById<TextView>(Resource.Id.textView3);
        }
    }
}