﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;
using RestSharp;
using CDSCMobileApp.DROID;
using Newtonsoft.Json;
using Android.Graphics.Drawables;
using Android.Graphics;
using CDSCMobileApp.Model;
using Android.Support.V7.Widget;
using CDSCMobileeApp.DROID.Fragments;

namespace CDSCMobileApp.Droid.Fragments
{
    public class Subscribe : Fragment
    {
        private List<string> products = new List<string>();
        private List<string> productss = new List<string>();
        RecyclerView.LayoutManager mLayoutManager;
        Spinner sbu;
        EditText membernumber, loanamt;
        Button submit;
       RecyclerView neme;
        ProdAdapter adapter;
        List<Productss> mPhotoAlbum;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public static Subscribe NewInstance()
        {
            var frag1 = new Subscribe { Arguments = new Bundle() };
            return frag1;
        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {var ignored = base.OnCreateView(inflater, container, savedInstanceState);

            View view = inflater.Inflate(Resource.Layout.Subscribe, null);
            this.Activity.SetTitle(Resource.String.subscribe);
            UserActivity.title = "SUBSCRIPTIONS";
            Intent downloadIntent = new Intent(this.Activity, typeof(AppUsageIntent));

            // This is just one example of passing some values to an IntentService via the Intent:
            this.Activity.StartService(downloadIntent);
            sbu = (Spinner)view.FindViewById(Resource.Id.productss);
            neme = view.FindViewById<RecyclerView>(Resource.Id.recyclerView);
            submit = (Button)view.FindViewById(Resource.Id.button1);

            try
            {	        
		
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
          products.Add("--Select Product--");
            try
            {
                var client = new RestClient(UserActivity.baseurl);
                var request = new RestRequest("Products", Method.GET);
                // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
                IRestResponse response = client.Execute(request);
                // request.AddBody(new tblMeetings {  });
                string validate = response.Content;


                List<Products> dataList = JsonConvert.DeserializeObject<List<Products>>(validate);

                var dbsel = from s in dataList
                            select s;

                foreach (var p in dbsel)
                {
                    products.Add(p.Name);
                }

            }
            catch (System.Exception)
            {


            }
            //get subscribed products
            var adapter1 = new ArrayAdapter<string>(this.Activity, Android.Resource.Layout.SimpleSpinnerItem, products);

            sbu.Adapter = adapter1;


                mLayoutManager = new LinearLayoutManager(this.Activity);
                neme.SetLayoutManager(mLayoutManager);

                var client2 = new RestClient(UserActivity.baseurl);
                var request2 = new RestRequest("Subscribed/{s}", Method.GET);
                // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
                request2.AddUrlSegment("s", UserActivity.member);
                IRestResponse response2 = client2.Execute(request2);
                // request.AddBody(new tblMeetings {  });
                string validate2 = response2.Content;


                List<Products> dataList2 = JsonConvert.DeserializeObject<List<Products>>(validate2);
                List<Productss> news = new List<Productss>();
                var dbsel2 = from s in dataList2
                             select s;

                int n = 0;
                foreach (var p in dbsel2)
                {

                    news.Add(new Productss { id = n, Name = p.Name });
                    n += 1;
                }

                Drawable[] imageId = new Drawable[news.Count];
                int x = 0;
                TextDrawable.TextDrawable drawable;
                foreach (var p in news)
                {
                    //TextDrawable.TextDrawable drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BuildRound("ABC", GetRandomColor(), GetRandomColor());
                    try
                    {
                        drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BeginConfig().FontSize(20).TextColor(Color.Black).Bold().EndConfig().BuildRound(p.Name.Substring(1), GetRandomColor(), GetRandomColor());

                        imageId[p.id] = drawable;
                    }
                    catch (Exception)
                    {

                        continue;
                    }

                }
                mPhotoAlbum = news;
                adapter = new ProdAdapter(mPhotoAlbum, imageId);
                neme.SetAdapter(adapter);

                submit.Click += Submit_Click;
         
	}
	catch (Exception g)
	{

                Toast.MakeText(this.Activity, "No internet or wifi", ToastLength.Short).Show();
                
            }
            return view;
        }

        private void Submit_Click(object sender, EventArgs e)
        {
            if (sbu.SelectedItem.ToString() == "--Select Product--")
            {
                Toast.MakeText(this.Activity, "Please select a product", ToastLength.Short).Show();
                return;
            }else
            {
                string nm = sbu.SelectedItem.ToString();
                var client = new RestClient(UserActivity.baseurl);
                var request = new RestRequest("Sub/{mem}/{toAdd}/{toRemove}", Method.POST);
                // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
                request.AddUrlSegment("mem",UserActivity.member);
                request.AddUrlSegment("toAdd",nm.Replace(" ",""));
                request.AddUrlSegment("toRemove", nm.Replace(" ", ""));
            
                IRestResponse response = client.Execute(request);
                // request.AddBody(new tblMeetings {  });
                string validate = response.Content;

                validate = validate.Replace(@"""", "");
                Toast.MakeText(this.Activity,validate, ToastLength.Short).Show();


                mLayoutManager = new LinearLayoutManager(this.Activity);
                neme.SetLayoutManager(mLayoutManager);

                var client2 = new RestClient(UserActivity.baseurl);
                var request2 = new RestRequest("Subscribed/{s}", Method.GET);
                // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
                request2.AddUrlSegment("s", UserActivity.member);
                IRestResponse response2 = client2.Execute(request2);
                // request.AddBody(new tblMeetings {  });
                string validate2 = response2.Content;


                List<Products> dataList2 = JsonConvert.DeserializeObject<List<Products>>(validate2);
                List<Productss> news = new List<Productss>();
                var dbsel2 = from s in dataList2
                             select s;

                int n = 0;
                foreach (var p in dbsel2)
                {

                    news.Add(new Productss { id = n, Name = p.Name });
                    n += 1;
                }

                Drawable[] imageId = new Drawable[news.Count];
                int x = 0;
                TextDrawable.TextDrawable drawable;
                foreach (var p in news)
                {
                    //TextDrawable.TextDrawable drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BuildRound("ABC", GetRandomColor(), GetRandomColor());
                    try
                    {
                        drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BeginConfig().FontSize(20).TextColor(Color.Black).Bold().EndConfig().BuildRound(p.Name.Substring(1), GetRandomColor(), GetRandomColor());

                        imageId[p.id] = drawable;
                    }
                    catch (Exception)
                    {

                        continue;
                    }

                }
                mPhotoAlbum = news;
                adapter = new ProdAdapter(mPhotoAlbum, imageId);
                neme.SetAdapter(adapter);

            }
        }

        static Random rand = new Random();
        public static Color GetRandomColor()
        {
            int hue = rand.Next(255);
            Color color = Color.HSVToColor(
                new[] {
            hue,
            1.0f,
            1.0f,
                }
            );
            return color;
        }
    }
}