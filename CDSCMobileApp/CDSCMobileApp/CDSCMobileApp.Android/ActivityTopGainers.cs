﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using CDSCMobileeApp.DROID.Fragments;
using Android.Graphics.Drawables;
using Android.Graphics;
using System.Xml.Serialization;
using System.IO;
using RestSharp;
using System.Net;

namespace CDSCMobileApp.Droid
{
    [Activity(Label = "TOP GAINERS", Theme = "@style/AppTheme2", MainLauncher = false, Icon = "@drawable/ctrademobile")]
    public class ActivityTopGainers : AppCompatActivity
    {
        RecyclerView newn;
        RecyclerView.LayoutManager mLayoutManager;
        PriceAdapter mAdapter;
        List<DATASETPRICES> mPhotoAlbum;
        public static string baseurl;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.ActivityPriceNotification);
            newn = this.FindViewById<RecyclerView>(Resource.Id.recyclerView);

             baseurl = "http://197.248.8.104/CDSCW";
            
            try 
	{	        
		// Plug in the linear layout manager:
            mLayoutManager = new LinearLayoutManager(this);
            newn.SetLayoutManager(mLayoutManager);
            var client = new RestClient(baseurl);
            var request = new RestRequest("dtpricesG", Method.GET);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            if (validate.Contains("dATASET_PRICESs") ==false)
            {
               // validate = @"<dATASET_PRICESs><DATASETPRICES><AVERAGE_PRICE>21.75</AVERAGE_PRICE><CLOSE_PRICE>21.75</CLOSE_PRICE><ISIN>KE0000000232</ISIN><ISSUER_CODE>FIRE</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>32</num_of_trades><PRICE_HI>23</PRICE_HI><PRICE_LOW>20.5</PRICE_LOW><SHARE_VOLUME>30000</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>653656.5</TURNOVER><trade_date>01-02-2006</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>45.75</AVERAGE_PRICE><CLOSE_PRICE>44.5</CLOSE_PRICE><ISIN>KE0000000034</ISIN><ISSUER_CODE>ARM</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>64</num_of_trades><PRICE_HI>47</PRICE_HI><PRICE_LOW>44.5</PRICE_LOW><SHARE_VOLUME>112583</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>5171493</TURNOVER><trade_date>01-02-2006</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>139</AVERAGE_PRICE><CLOSE_PRICE>136</CLOSE_PRICE><ISIN>KE0000000323</ISIN><ISSUER_CODE>KENO</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>15</num_of_trades><PRICE_HI>140</PRICE_HI><PRICE_LOW>135</PRICE_LOW><SHARE_VOLUME>58769</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>8214368</TURNOVER><trade_date>01-02-2006</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>51</AVERAGE_PRICE><CLOSE_PRICE>51</CLOSE_PRICE><ISIN>KE0000000133</ISIN><ISSUER_CODE>CMC</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>23</num_of_trades><PRICE_HI>52.5</PRICE_HI><PRICE_LOW>50.5</PRICE_LOW><SHARE_VOLUME>72279</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>3715929</TURNOVER><trade_date>01-02-2006</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>135</AVERAGE_PRICE><CLOSE_PRICE>133</CLOSE_PRICE><ISIN>KE0000000349</ISIN><ISSUER_CODE>KPLC</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>17</num_of_trades><PRICE_HI>136</PRICE_HI><PRICE_LOW>133</PRICE_LOW><SHARE_VOLUME>22229</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>3009323</TURNOVER><trade_date>01-02-2006</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>135</AVERAGE_PRICE><CLOSE_PRICE>133</CLOSE_PRICE><ISIN>KE4000001877</ISIN><ISSUER_CODE>KPLC</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>17</num_of_trades><PRICE_HI>136</PRICE_HI><PRICE_LOW>133</PRICE_LOW><SHARE_VOLUME>22229</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>3009323</TURNOVER><trade_date>01-02-2006</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>135</AVERAGE_PRICE><CLOSE_PRICE>133</CLOSE_PRICE><ISIN>KE2000001335</ISIN><ISSUER_CODE>KPLC</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>17</num_of_trades><PRICE_HI>136</PRICE_HI><PRICE_LOW>133</PRICE_LOW><SHARE_VOLUME>22229</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>3009323</TURNOVER><trade_date>01-02-2006</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>135</AVERAGE_PRICE><CLOSE_PRICE>133</CLOSE_PRICE><ISIN>KE4000002982</ISIN><ISSUER_CODE>KPLC</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>17</num_of_trades><PRICE_HI>136</PRICE_HI><PRICE_LOW>133</PRICE_LOW><SHARE_VOLUME>22229</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>3009323</TURNOVER><trade_date>01-02-2006</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>44</AVERAGE_PRICE><CLOSE_PRICE>44.5</CLOSE_PRICE><ISIN>KE0000000281</ISIN><ISSUER_CODE>KUKZ</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>4</num_of_trades><PRICE_HI>45.5</PRICE_HI><PRICE_LOW>44</PRICE_LOW><SHARE_VOLUME>13300</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>586850</TURNOVER><trade_date>01-02-2005</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>96</AVERAGE_PRICE><CLOSE_PRICE>95.5</CLOSE_PRICE><ISIN>KE0000000059</ISIN><ISSUER_CODE>BAMB</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>19</num_of_trades><PRICE_HI>97.5</PRICE_HI><PRICE_LOW>95.5</PRICE_LOW><SHARE_VOLUME>30000</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>2888550</TURNOVER><trade_date>01-02-2005</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>39.5</AVERAGE_PRICE><CLOSE_PRICE>40</CLOSE_PRICE><ISIN>KE0000000638</ISIN><ISSUER_CODE>DTK</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>42</num_of_trades><PRICE_HI>40.75</PRICE_HI><PRICE_LOW>39</PRICE_LOW><SHARE_VOLUME>179129</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>7096607</TURNOVER><trade_date>01-02-2006</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>39.5</AVERAGE_PRICE><CLOSE_PRICE>40</CLOSE_PRICE><ISIN>KE0000000158</ISIN><ISSUER_CODE>DTK</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>42</num_of_trades><PRICE_HI>40.75</PRICE_HI><PRICE_LOW>39</PRICE_LOW><SHARE_VOLUME>179129</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>7096607</TURNOVER><trade_date>01-02-2006</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>75</AVERAGE_PRICE><CLOSE_PRICE>75</CLOSE_PRICE><ISIN>KE0000000265</ISIN><ISSUER_CODE>ICDC</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>9</num_of_trades><PRICE_HI>76</PRICE_HI><PRICE_LOW>75</PRICE_LOW><SHARE_VOLUME>19697</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>1478275</TURNOVER><trade_date>01-02-2006</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>115</AVERAGE_PRICE><CLOSE_PRICE>116</CLOSE_PRICE><ISIN>KE1000001485</ISIN><ISSUER_CODE>KCB</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>105</num_of_trades><PRICE_HI>117</PRICE_HI><PRICE_LOW>114</PRICE_LOW><SHARE_VOLUME>82688</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>9564085</TURNOVER><trade_date>01-02-2006</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>115</AVERAGE_PRICE><CLOSE_PRICE>116</CLOSE_PRICE><ISIN>KE0000000315</ISIN><ISSUER_CODE>KCB</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>105</num_of_trades><PRICE_HI>117</PRICE_HI><PRICE_LOW>114</PRICE_LOW><SHARE_VOLUME>82688</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>9564085</TURNOVER><trade_date>01-02-2006</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>30</AVERAGE_PRICE><CLOSE_PRICE>30</CLOSE_PRICE><ISIN>KE0000000430</ISIN><ISSUER_CODE>SASN</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>4</num_of_trades><PRICE_HI>30.25</PRICE_HI><PRICE_LOW>30</PRICE_LOW><SHARE_VOLUME>5300</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>159125</TURNOVER><trade_date>01-02-2005</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>14</AVERAGE_PRICE><CLOSE_PRICE>13.5</CLOSE_PRICE><ISIN>KE0000000232</ISIN><ISSUER_CODE>FIRE</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>3</num_of_trades><PRICE_HI>13.6</PRICE_HI><PRICE_LOW>13.5</PRICE_LOW><SHARE_VOLUME>1650</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>22395</TURNOVER><trade_date>01-02-2005</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>38</AVERAGE_PRICE><CLOSE_PRICE>38</CLOSE_PRICE><ISIN>KE0000000141</ISIN><ISSUER_CODE>BERG</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>5</num_of_trades><PRICE_HI>38.25</PRICE_HI><PRICE_LOW>38</PRICE_LOW><SHARE_VOLUME>19000</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>722125</TURNOVER><trade_date>01-02-2006</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>20</AVERAGE_PRICE><CLOSE_PRICE>19.5</CLOSE_PRICE><ISIN>KE2000003323</ISIN><ISSUER_CODE>KQ</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>28</num_of_trades><PRICE_HI>19.55</PRICE_HI><PRICE_LOW>19.5</PRICE_LOW><SHARE_VOLUME>132709</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>2588579.45</TURNOVER><trade_date>01-02-2005</trade_date></DATASETPRICES><DATASETPRICES><AVERAGE_PRICE>20</AVERAGE_PRICE><CLOSE_PRICE>19.5</CLOSE_PRICE><ISIN>KE0000000307</ISIN><ISSUER_CODE>KQ</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><num_of_trades>28</num_of_trades><PRICE_HI>19.55</PRICE_HI><PRICE_LOW>19.5</PRICE_LOW><SHARE_VOLUME>132709</SHARE_VOLUME><SUB_TYPE>0000</SUB_TYPE><TURNOVER>2588579.45</TURNOVER><trade_date>01-02-2005</trade_date></DATASETPRICES></dATASET_PRICESs>";
            }
            //Console.WriteLine(validate);
            dATASET_PRICESs result = null;
            List<dATASET_PRICESs> nn = null;
            XmlSerializer serializer = new XmlSerializer(typeof(dATASET_PRICESs));
            using (TextReader reader = new StringReader(validate))
            {
                try
                {
                    result = (dATASET_PRICESs)serializer.Deserialize(reader);

                }
                catch (Exception)
                {


                }
            }

            Drawable[] imageId = new Drawable[result.DATASETPRICESs.Count];
            int x = 0;
            TextDrawable.TextDrawable drawable;
            foreach (var d in result.DATASETPRICESs)
            {

                try
                {
                    //TextDrawable.TextDrawable drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BuildRound("ABC", GetRandomColor(), GetRandomColor());
                    drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BeginConfig().FontSize(28).TextColor(Color.Black).Bold().EndConfig().BuildRoundRect(d.ISSUER_CODE, GetRandomColor(),2, GetRandomColor());
                    imageId[x] = drawable;
                }
                catch (Exception)
                {
                    continue;
                }
                x += 1;
            }
            mPhotoAlbum = result.DATASETPRICESs;
            int my = mPhotoAlbum.Count;
            // Plug in my adapter:
            mAdapter = new PriceAdapter(mPhotoAlbum, imageId);
            newn.SetAdapter(mAdapter);

	}
	catch (Exception e)
	{

                Toast.MakeText(this, "No internet or wifi", ToastLength.Short).Show();
                return;
            }
        }
        static Random rand = new Random();
        public static Color GetRandomColor()
        {
            int hue = rand.Next(255);
            Color color = Color.HSVToColor(
                new[] {
            hue,
            1.0f,
            1.0f,
                }
            );
            return color;
        }
        public bool IsAddressAvailable(string address)
        {

            Boolean isHttpOk;
            try
            {
                Stream sStream;
                HttpWebRequest urlReq;
                HttpWebResponse urlRes;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(address);
                req.Method = "HEAD";
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                isHttpOk = resp.StatusCode == HttpStatusCode.OK;
                resp.Close();

                return isHttpOk;
            }
            catch (Exception)
            {
                isHttpOk = false;
                return isHttpOk;
            }
        }

    }
}