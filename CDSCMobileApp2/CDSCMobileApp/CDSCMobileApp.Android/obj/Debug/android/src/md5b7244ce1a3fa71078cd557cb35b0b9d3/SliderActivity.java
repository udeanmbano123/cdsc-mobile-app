package md5b7244ce1a3fa71078cd557cb35b0b9d3;


public class SliderActivity
	extends android.support.v7.app.AppCompatActivity
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("CDSCMobile.DROID.SliderActivity, CDSCMobileApp.Android, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", SliderActivity.class, __md_methods);
	}


	public SliderActivity () throws java.lang.Throwable
	{
		super ();
		if (getClass () == SliderActivity.class)
			mono.android.TypeManager.Activate ("CDSCMobile.DROID.SliderActivity, CDSCMobileApp.Android, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
