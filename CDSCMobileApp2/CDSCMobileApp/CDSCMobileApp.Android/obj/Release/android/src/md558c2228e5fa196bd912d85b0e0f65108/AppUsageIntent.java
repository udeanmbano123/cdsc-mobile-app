package md558c2228e5fa196bd912d85b0e0f65108;


public class AppUsageIntent
	extends mono.android.app.IntentService
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onHandleIntent:(Landroid/content/Intent;)V:GetOnHandleIntent_Landroid_content_Intent_Handler\n" +
			"";
		mono.android.Runtime.register ("CDSCMobileApp.Droid.AppUsageIntent, CDSCMobileApp.Android, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", AppUsageIntent.class, __md_methods);
	}


	public AppUsageIntent (java.lang.String p0) throws java.lang.Throwable
	{
		super (p0);
		if (getClass () == AppUsageIntent.class)
			mono.android.TypeManager.Activate ("CDSCMobileApp.Droid.AppUsageIntent, CDSCMobileApp.Android, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "System.String, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e", this, new java.lang.Object[] { p0 });
	}


	public AppUsageIntent () throws java.lang.Throwable
	{
		super ();
		if (getClass () == AppUsageIntent.class)
			mono.android.TypeManager.Activate ("CDSCMobileApp.Droid.AppUsageIntent, CDSCMobileApp.Android, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onHandleIntent (android.content.Intent p0)
	{
		n_onHandleIntent (p0);
	}

	private native void n_onHandleIntent (android.content.Intent p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
