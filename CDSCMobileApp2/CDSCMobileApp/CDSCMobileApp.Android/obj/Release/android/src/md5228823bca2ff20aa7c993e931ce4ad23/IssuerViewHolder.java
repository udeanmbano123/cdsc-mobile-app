package md5228823bca2ff20aa7c993e931ce4ad23;


public class IssuerViewHolder
	extends android.support.v7.widget.RecyclerView.ViewHolder
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("CDSCMobileeApp.DROID.Fragments.IssuerViewHolder, CDSCMobileApp.Android, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", IssuerViewHolder.class, __md_methods);
	}


	public IssuerViewHolder (android.view.View p0) throws java.lang.Throwable
	{
		super (p0);
		if (getClass () == IssuerViewHolder.class)
			mono.android.TypeManager.Activate ("CDSCMobileeApp.DROID.Fragments.IssuerViewHolder, CDSCMobileApp.Android, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Views.View, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
