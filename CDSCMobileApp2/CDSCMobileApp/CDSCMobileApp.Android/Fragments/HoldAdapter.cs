﻿using System;
using System.Collections.Generic;
using Android.Support.V7.Widget;
using Android.Views;
using CDSCMobileApp;
using Android.Widget;
using CDSCMobileApp.Droid;
using Android.Graphics.Drawables;
using System.Globalization;

namespace CDSCMobileeApp.DROID.Fragments
{

    public class HoldAdapter : RecyclerView.Adapter
    {
       // public dATASET_PRICESs mPhotoAlbum;
        public List<DATASETHOLDINGS> mPhotoAlbum;
        private Drawable[] imageId;
        public HoldAdapter(List<DATASETHOLDINGS> photoAlbum, Drawable[] imageId)
        {
            mPhotoAlbum = photoAlbum;
            this.imageId = imageId;
        }

        public override RecyclerView.ViewHolder
            OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).
                        Inflate(Resource.Layout.HoldCardView, parent, false);
            HoldViewHolder vh = new HoldViewHolder(itemView);
            return vh;
        }

        public override void
            OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            HoldViewHolder vh = holder as HoldViewHolder;
            vh.Image.SetImageDrawable(imageId[position]);
            vh.Image.Visibility = ViewStates.Invisible;
            vh.Caption2.Text =mPhotoAlbum[position].SHORT_NAME;
            vh.Caption.Text ="DATE:" + Convert.ToDateTime(mPhotoAlbum[position].DATE_LAST_TRANSACTION).ToString("dd-MMM-yyyy");
            vh.Caption3.Text ="QUANTITY:" + mPhotoAlbum[position].qnty_held;
        }

        public override int ItemCount
        {
            get { return mPhotoAlbum.Count; }
        }
    }
    public class HoldViewHolder : RecyclerView.ViewHolder
    {
        public ImageView Image { get; private set; }
        public TextView Caption { get; private set; }
        public TextView Caption2 { get; private set; }
        public TextView Caption3 { get; private set; }

        public HoldViewHolder(View itemView) : base(itemView)
        {
            // Locate and cache view references:
           Image = itemView.FindViewById<ImageView>(Resource.Id.imageView);
           Caption = itemView.FindViewById<TextView>(Resource.Id.textView);
           Caption2 = itemView.FindViewById<TextView>(Resource.Id.textView2);
          Caption3 = itemView.FindViewById<TextView>(Resource.Id.textView3);
        }
    }
}