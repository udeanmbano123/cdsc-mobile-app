﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;
using RestSharp;

namespace CDSCMobileApp.Droid.Fragments
{
    public class Deactivate : Fragment
    {
        RadioButton yes, no;
        Button login;
        public static Deactivate NewInstance()
        {
            var frag1 = new Deactivate { Arguments = new Bundle() };
            return frag1;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);

            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            this.Activity.SetTitle(Resource.String.deactivate);
            UserActivity.title = "DEACTIVATE ACCOUNT";
            Intent downloadIntent = new Intent(this.Activity, typeof(AppUsageIntent));

            // This is just one example of passing some values to an IntentService via the Intent:
            this.Activity.StartService(downloadIntent);
            View view = inflater.Inflate(Resource.Layout.DeactivateAccount, null);
            login = view.FindViewById<Button>(Resource.Id.btnLogin);
            yes = view.FindViewById<RadioButton>(Resource.Id.PMEC);
            no = view.FindViewById<RadioButton>(Resource.Id.Bankers);

            login.Click += Login_Click;
            return view;
        }

        private void Login_Click(object sender, EventArgs e)
        {
            Boolean yesnno;

            Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this.Activity);

            if (yes.Checked==true)
            {

                Toast.MakeText(this.Activity, "Account deactivation in process", ToastLength.Short).Show();
                //deactivate
                var client = new RestClient(UserActivity.baseurl);
                var request = new RestRequest("Deactivate/{s}", Method.POST);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request.AddUrlSegment("s", UserActivity.member);
                IRestResponse response = client.Execute(request);
                string validate = response.Content;
                validate = validate.Replace(@"""", "");
                //Processing logout
                alert.SetPositiveButton("OK", (senderAlert, args) => {
                    var intent = new Intent(Activity, typeof(MainActivity));
                    StartActivity(intent);
                });
                alert.SetTitle("Notification");
                alert.SetMessage(validate);
                Android.App.Dialog dialog = alert.Create();
                dialog.Show();
              

            }
            else
            {
                FragmentManager fm = this.FragmentManager;
                var trans = fm.BeginTransaction();
                trans.Replace(Resource.Id.content_frame, Fragment1.NewInstance(), "Fragment1");
                trans.AddToBackStack("Deactivate");
                trans.Commit();
            }
        }
    }
}