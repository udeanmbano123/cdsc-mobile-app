﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using CDSCMobileeApp.DROID.Fragments;
using System.Xml.Serialization;
using System.IO;
using Android.Graphics.Drawables;
using Android.Graphics;
using Android.Support.V4.App;
using RestSharp;
using Android.Support.V7.App;

namespace CDSCMobileApp.Droid.Fragments
{
    public class Escrow : Fragment
    {
        RecyclerView newn;
        RecyclerView.LayoutManager mLayoutManager;
        EscrowAdapter mAdapter;
        List<ESCROWCLIENTS> mPhotoAlbum;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
           
           
            // Create your fragment here
        }
        public static Escrow NewInstance()
        {
            var frag1 = new Escrow { Arguments = new Bundle() };
            return frag1;
        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
           this.Activity.SetTitle(Resource.String.accountupdate);
            Intent downloadIntent = new Intent(this.Activity, typeof(AppUsageIntent));
            UserActivity.title = "ACCOUNT UPDATES";
            // This is just one example of passing some values to an IntentService via the Intent:
            this.Activity.StartService(downloadIntent);

            View view = inflater.Inflate(Resource.Layout.ActivityPriceNotification, null);
            newn= view.FindViewById<RecyclerView>(Resource.Id.recyclerView);
try 
	{	        
		
            // Plug in the linear layout manager:
            mLayoutManager = new LinearLayoutManager(this.Activity);
            newn.SetLayoutManager(mLayoutManager);
            var client = new RestClient(UserActivity.baseurl);
            var request = new RestRequest("dtclients/{s}", Method.GET);
            request.AddUrlSegment("s",UserActivity.member);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
           
            if (validate.Contains("eSCROW_CLIENTSs") ==false)
            {
               // validate = @"<eSCROW_CLIENTSs><ESCROWCLIENTS><CLIENT_PREFIX>0000000000450</CLIENT_PREFIX><CLIENT_SUFFIX>LI</CLIENT_SUFFIX><CLIENT_TYPE>NM</CLIENT_TYPE><DATE_CHANGED>29-09-2008</DATE_CHANGED><DATE_OF_REGISTRATION>22-11-2004</DATE_OF_REGISTRATION><MEMBER_CODE>B02</MEMBER_CODE><MEMBER_TYPE>B</MEMBER_TYPE><OTHER_NAMES>JOFIDHA  MAKOWENGA</OTHER_NAMES><SURNAME>OTIENO</SURNAME></ESCROWCLIENTS><ESCROWCLIENTS><CLIENT_PREFIX>0000000000450</CLIENT_PREFIX><CLIENT_SUFFIX>LI</CLIENT_SUFFIX><CLIENT_TYPE>NM</CLIENT_TYPE><DATE_CHANGED>29-09-2008</DATE_CHANGED><DATE_OF_REGISTRATION>11-11-2004</DATE_OF_REGISTRATION><MEMBER_CODE>B07</MEMBER_CODE><MEMBER_TYPE>B</MEMBER_TYPE><OTHER_NAMES>JOFIDHA  MAKOWENGA</OTHER_NAMES><SURNAME>OTIENO</SURNAME></ESCROWCLIENTS></eSCROW_CLIENTSs>";

            }
            //Console.WriteLine(validate);
            eSCROW_CLIENTSs result = null;
            List<eSCROW_CLIENTSs> nn = null;
            XmlSerializer serializer = new XmlSerializer(typeof(eSCROW_CLIENTSs));
            using (TextReader reader = new StringReader(validate))
            {
                try
                {
                    result = (eSCROW_CLIENTSs)serializer.Deserialize(reader);

                }
                catch (Exception)
                {


                }
            }

            Drawable[] imageId = new Drawable[result.eSCROW_CLIENTSss.Count];
            int x = 0;
            TextDrawable.TextDrawable drawable;
            foreach (var d in result.eSCROW_CLIENTSss)
            {

                try
                {
                    //TextDrawable.TextDrawable drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BuildRound("ABC", GetRandomColor(), GetRandomColor());
                    drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BeginConfig().FontSize(28).TextColor(Color.Black).Bold().EndConfig().BuildRound(d.MEMBER_CODE, GetRandomColor(), GetRandomColor());
                    imageId[x] = drawable;
                }
                catch (Exception)
                {
                    continue;
                }
                x += 1;
            }
            mPhotoAlbum = result.eSCROW_CLIENTSss;
            int my = mPhotoAlbum.Count;
            // Plug in my adapter:
            mAdapter = new EscrowAdapter(mPhotoAlbum, imageId);
            newn.SetAdapter(mAdapter);

	}
	catch (Exception f)
	{

                Toast.MakeText(this.Activity, "No internet or wifi", ToastLength.Short).Show();

            }
            return view;
        }
        static Random rand = new Random();
        public static Color GetRandomColor()
        {
            int hue = rand.Next(255);
            Color color = Color.HSVToColor(
                new[] {
            hue,
            1.0f,
            1.0f,
                }
            );
            return color;
        }
    }
}