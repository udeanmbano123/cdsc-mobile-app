using System;
using Android.OS;
using Android.Views;
using Android.Widget;
using RestSharp;
using RestSharp.Extensions.MonoHttp;
using Android.Support.V4.App;
using Android.Content;

namespace CDSCMobileApp.Droid.Fragments
{
    public class Fragment5 : Fragment
    {

        Button btn,btn2;
        View view;
        EditText ticket;
        EditText mess;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }
        public static Fragment5 NewInstance()
        {
            var frag1 = new Fragment5 { Arguments = new Bundle() };
            return frag1;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
           view=inflater.Inflate(Resource.Layout.fragment5, container, false);
            UserActivity.title = "TICKET UPDATE";
            this.Activity.SetTitle(Resource.String.tickupdt);
            Intent downloadIntent = new Intent(this.Activity, typeof(AppUsageIntent));

            // This is just one example of passing some values to an IntentService via the Intent:
            this.Activity.StartService(downloadIntent);
            btn = view.FindViewById<Button>(Resource.Id.button1);
            btn2 = view.FindViewById<Button>(Resource.Id.button2);
            ticket = view.FindViewById<EditText>(Resource.Id.ticketref);
            mess = view.FindViewById<EditText>(Resource.Id.message);
            btn.Click += Btn_Click;
            btn2.Click += Btn2_Click;
            return view;
        }

        private void Btn2_Click(object sender, EventArgs e)
        {
            FragmentManager fm = this.FragmentManager;
            var trans = fm.BeginTransaction();
            fm.PopBackStack();
           // trans.Commit();
        }

        private void Btn_Click(object sender, EventArgs e)
        {
            if (ticket.Text == "")
            {
                Toast.MakeText(this.Activity, "Ticket is required", ToastLength.Short).Show();

                return;

            }
            else if (mess.Text == "")
            {
                Toast.MakeText(this.Activity, "Message is required", ToastLength.Short).Show();

                return;
            }
            else
            {
                Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(this.Activity);
                Android.App.AlertDialog alertDialog = builder.Create();
                alertDialog.SetTitle("Exit Message");
                alertDialog.SetIcon(Resource.Drawable.Icon);
                alertDialog.SetMessage("Are you sure you want to submit your ticket update");

                //NO
                alertDialog.SetButton("No", (s, ev) =>
                {
                    //DO Something

                    Toast.MakeText(this.Activity, "Please take your time to verify your details", ToastLength.Short).Show();
                    return;
                });

                //YES
                alertDialog.SetButton2("Yes", (s, ev) =>
                {
                    alertDialog.Show();
                    Toast.MakeText(this.Activity, "Please be patient while processing request", ToastLength.Short).Show();

                    string tickets = ticket.Text;
                    string actions = HttpUtility.UrlEncode(mess.Text).Replace("+", "%20");

                    var baseUrl = "http://166.63.10.232/OldMutual";
                    var client = new RestClient(baseUrl);
                    var request = new RestRequest("tickets/{ticket}/{response}", Method.GET);
                    request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                    request.AddUrlSegment("ticket", tickets.Replace(" ", string.Empty));
                    request.AddUrlSegment("response", actions.Replace(".", "%2E"));
                    IRestResponse response = client.Execute(request);
                    string validate = response.Content;

                    validate = validate.Replace(@"""", "");

                    if (validate == "Success")
                    {
                        Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this.Activity);
                        alert.SetTitle("Notification");
                        alert.SetMessage(validate + " Thank you for updating us on your ticket expect feedback from us very soon.Within 24 hours");
                        ticket.Text = "";
                        mess.Text = "";

                        alert.SetPositiveButton("OK", (senderAlert, args) => {
                            //FragmentManager fm = this.FragmentManager;
                            // //fm.PopBackStack(0,0);
                            // var trans = fm.BeginTransaction();
                            // fm.PopBackStack();
                        });
                        Android.App.Dialog dialog = alert.Create();
                        dialog.Show();
                    }
                    else
                    {
                        Toast.MakeText(this.Activity, validate, ToastLength.Short).Show();
                        return;
                    }
                });



                alertDialog.Show();




            }

          
        }
    }
}