﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using CDSCMobileeApp.DROID.Fragments;
using System.Xml.Serialization;
using System.IO;
using Android.Graphics.Drawables;
using Android.Graphics;
using Android.Support.V4.App;
using RestSharp;

namespace CDSCMobileApp.Droid.Fragments
{
    public class Holdings : Fragment
    {
        RecyclerView newn;
        RecyclerView.LayoutManager mLayoutManager;
        HoldAdapter mAdapter;
        List<DATASETHOLDINGS> mPhotoAlbum;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }
        public static Holdings NewInstance()
        {
            var frag1 = new Holdings { Arguments = new Bundle() };
            return frag1;
        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            this.Activity.SetTitle(Resource.String.holdings);
            Intent downloadIntent = new Intent(this.Activity, typeof(AppUsageIntent));
            UserActivity.title = "HOLDINGS";
            // This is just one example of passing some values to an IntentService via the Intent:
            this.Activity.StartService(downloadIntent);

            View view = inflater.Inflate(Resource.Layout.ActivityPriceNotification, null);
            newn= view.FindViewById<RecyclerView>(Resource.Id.recyclerView);
try 
	{	        
		
            // Plug in the linear layout manager:
            mLayoutManager = new LinearLayoutManager(this.Activity);
            newn.SetLayoutManager(mLayoutManager);
            var client = new RestClient(UserActivity.baseurl);
            var request = new RestRequest("/dthold/{s}", Method.GET);
            request.AddUrlSegment("s", UserActivity.member);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;
           
            if (validate.Contains(" dATASET_HOLDINGSs") ==false)
            {
                // = @"<dATASET_HOLDINGSs><DATASETHOLDINGS><CLIENT_PREFIX>0000000000450</CLIENT_PREFIX><CLIENT_SUFFIX>LI</CLIENT_SUFFIX><DATE_LAST_TRANSACTION>28-06-2013</DATE_LAST_TRANSACTION><ISSUER_CODE>NMG</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><MEMBER_CODE>B07</MEMBER_CODE><MEMBER_TYPE>B</MEMBER_TYPE><qnty_held>79</qnty_held><SHORT_NAME>SUNTRA INV. BANK</SHORT_NAME><SUB_TYPE>0000</SUB_TYPE><sec_ACC_TYPE>BALANCE FREE</sec_ACC_TYPE></DATASETHOLDINGS><DATASETHOLDINGS><CLIENT_PREFIX>0000000000450</CLIENT_PREFIX><CLIENT_SUFFIX>LI</CLIENT_SUFFIX><DATE_LAST_TRANSACTION>28-09-2012</DATE_LAST_TRANSACTION><ISSUER_CODE>UCHM</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><MEMBER_CODE>B02</MEMBER_CODE><MEMBER_TYPE>B</MEMBER_TYPE><qnty_held>59</qnty_held><SHORT_NAME>DYER&amp;BLAIR INV.BANK</SHORT_NAME><SUB_TYPE>0000</SUB_TYPE><sec_ACC_TYPE>BALANCE FREE</sec_ACC_TYPE></DATASETHOLDINGS><DATASETHOLDINGS><CLIENT_PREFIX>0000000000450</CLIENT_PREFIX><CLIENT_SUFFIX>LI</CLIENT_SUFFIX><DATE_LAST_TRANSACTION>30-03-2016</DATE_LAST_TRANSACTION><ISSUER_CODE>GLD</ISSUER_CODE><MAIN_TYPE>E</MAIN_TYPE><MEMBER_CODE>B02</MEMBER_CODE><MEMBER_TYPE>B</MEMBER_TYPE><qnty_held>49999900</qnty_held><SHORT_NAME>DYER&amp;BLAIR INV.BANK</SHORT_NAME><SUB_TYPE>0000</SUB_TYPE><sec_ACC_TYPE>BALANCE FREE</sec_ACC_TYPE></DATASETHOLDINGS><DATASETHOLDINGS><CLIENT_PREFIX>0000000000450</CLIENT_PREFIX><CLIENT_SUFFIX>LI</CLIENT_SUFFIX><DATE_LAST_TRANSACTION>30-10-2016</DATE_LAST_TRANSACTION><ISSUER_CODE>EXB</ISSUER_CODE><MAIN_TYPE>O</MAIN_TYPE><MEMBER_CODE>B02</MEMBER_CODE><MEMBER_TYPE>B</MEMBER_TYPE><qnty_held>50000000</qnty_held><SHORT_NAME>DYER&amp;BLAIR INV.BANK</SHORT_NAME><SUB_TYPE>0000</SUB_TYPE><sec_ACC_TYPE>BALANCE FREE</sec_ACC_TYPE></DATASETHOLDINGS></dATASET_HOLDINGSs>";

            }
            //Console.WriteLine(validate);
            dATASET_HOLDINGSs result = null;
            List<dATASET_HOLDINGSs> nn = null;
            XmlSerializer serializer = new XmlSerializer(typeof(dATASET_HOLDINGSs));
            using (TextReader reader = new StringReader(validate))
            {
                try
                {
                    result = (dATASET_HOLDINGSs)serializer.Deserialize(reader);

                }
                catch (Exception)
                {


                }
            }

            Drawable[] imageId = new Drawable[result.DATASETCDAs.Count];
            int x = 0;
            TextDrawable.TextDrawable drawable;
            foreach (var d in result.DATASETCDAs)
            {
                
                try
                {
                    //TextDrawable.TextDrawable drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BuildRound("ABC", GetRandomColor(), GetRandomColor());
                    drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BeginConfig().FontSize(28).TextColor(Color.Black).Bold().EndConfig().BuildRoundRect(d.ISSUER_CODE, GetRandomColor(),2, GetRandomColor());
                    imageId[x] = drawable;
                }
                catch (Exception)
                {
                    continue;
                }
                x += 1;
            }
            mPhotoAlbum = result.DATASETCDAs;
            int my = mPhotoAlbum.Count;
            // Plug in my adapter:
            mAdapter = new HoldAdapter(mPhotoAlbum, imageId);
            newn.SetAdapter(mAdapter);

	}
	catch (Exception f)
	{
                Toast.MakeText(this.Activity, "No internet or wifi", ToastLength.Short).Show();

            }
            return view;
        }
        static Random rand = new Random();
        public static Color GetRandomColor()
        {
            int hue = rand.Next(255);
            Color color = Color.HSVToColor(
                new[] {
            hue,
            1.0f,
            1.0f,
                }
            );
            return color;
        }
    }
}