using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using RestSharp;
using System;
using System.Net;
using System.Security.Cryptography;
using System.Text;

namespace CDSCMobileApp.Droid.Fragments
{
    public class ChangePassword : Fragment
    {
        EditText username, pass, pass2;
        Button btn, btn2;
        public static string baseurl { get; set; }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
           
                baseurl = "http://197.248.8.104/CDSCW";
           
            // Create your fragment here
        }

        public static ChangePassword NewInstance()
        {
            var frag1 = new ChangePassword { Arguments = new Bundle() };
            return frag1;
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            View view = inflater.Inflate(Resource.Layout.ChangePassword, null);
            this.Activity.SetTitle(Resource.String.changepassword);

            username = (EditText)view.FindViewById(Resource.Id.username);
            pass = (EditText)view.FindViewById(Resource.Id.password);
            pass2 = (EditText)view.FindViewById(Resource.Id.confirmpassword);
            btn = (Button)view.FindViewById(Resource.Id.btnLogin);
            btn2 = (Button)view.FindViewById(Resource.Id.btnClear);
            btn.Click += Btn_Click;
            username.Text = UserActivity.username;
            username.Enabled = false;
            btn2.Click += delegate
            {

                pass2.Text = "";
                pass.Text = "";
            };
            return view;
        }

        private void Btn_Click(object sender, System.EventArgs e)
        {
           if (pass.Text == "")
            {
                Toast.MakeText(this.Activity, "Password is required", ToastLength.Short).Show();
                return;

            }
            else if (pass2.Text == "")
            {
                Toast.MakeText(this.Activity, "Confirm password  is required", ToastLength.Short).Show();
                return;

            }
            else if (pass.Text != pass2.Text)
            {
                Toast.MakeText(this.Activity, "Passwords  do not match", ToastLength.Short).Show();
                return;

            }else
            {
                btn.Enabled = false;
                var client = new RestClient(baseurl);
                var request = new RestRequest("Change/{s}/{p}", Method.POST);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request.AddUrlSegment("s", UserActivity.member);
                request.AddUrlSegment("p", ComputeHash(pass.Text, new SHA256CryptoServiceProvider()));
                IRestResponse response = client.Execute(request);
                string validate = response.Content;

                validate = validate.Replace(@"""", "");
                string val = validate;
                Toast.MakeText(this.Activity, "Passswords changed successfully", ToastLength.Short).Show();
                return;
            }
        }
        public bool IsAddressAvailable(string address)
        {
            try
            {
                System.Net.WebClient client = new WebClient();
                client.DownloadData(address);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }
    }
}