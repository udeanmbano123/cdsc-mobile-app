﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;

namespace CDSCMobileApp.Droid.Fragments
{
    public class Payment : Fragment
    {
        public static Payment NewInstance()
        {
            var frag1 = new Payment { Arguments = new Bundle() };
            return frag1;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);

            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            this.Activity.SetTitle(Resource.String.payment);
            Intent downloadIntent = new Intent(this.Activity, typeof(AppUsageIntent));
            UserActivity.title = "PAYMENT";
            // This is just one example of passing some values to an IntentService via the Intent:
            this.Activity.StartService(downloadIntent);

            View view = inflater.Inflate(Resource.Layout.Pay, null);

            return view;
        }
    }
}