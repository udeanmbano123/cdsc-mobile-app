using System;
using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Android.Content;

namespace CDSCMobileApp.Droid.Fragments
{
    public class Fragment1 : Fragment
    {
    LinearLayout btn2, btn3,btn4,btn1,btn5,btn6,btn7,btn8,btn9,btn10;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public static Fragment1 NewInstance()
        {
            var frag1 = new Fragment1 { Arguments = new Bundle() };
            return frag1;
        }


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            this.Activity.SetTitle(Resource.String.home);
            Intent downloadIntent = new Intent(this.Activity, typeof(AppUsageIntent));
            UserActivity.title = "TICKET VIEW";
            // This is just one example of passing some values to an IntentService via the Intent:
            this.Activity.StartService(downloadIntent);

            View view = inflater.Inflate(Resource.Layout.fragment1, null);
            btn1 = view.FindViewById<LinearLayout>(Resource.Id.AccountUpdate);
            btn2 = view.FindViewById<LinearLayout>(Resource.Id.MiniStat);
            btn3 = view.FindViewById<LinearLayout>(Resource.Id.CDA);
            btn4 = view.FindViewById<LinearLayout>(Resource.Id.Hold);
            btn5 = view.FindViewById<LinearLayout>(Resource.Id.ISSUERS);
            btn6 = view.FindViewById<LinearLayout>(Resource.Id.PRICE);
            btn7 = view.FindViewById<LinearLayout>(Resource.Id.subscriptions);
            btn8 = view.FindViewById<LinearLayout>(Resource.Id.CustomerCall);
            btn9 = view.FindViewById<LinearLayout>(Resource.Id.Deactivate);
            btn10 = view.FindViewById<LinearLayout>(Resource.Id.Payments);

            btn1.Click += Btn1_Click;
            btn2.Click += Btn2_Click;
            btn3.Click += Btn3_Click;
            btn4.Click += Btn4_Click;
            btn5.Click += Btn5_Click;
            btn6.Click += Btn6_Click;
            btn7.Click += Btn7_Click;
            btn8.Click += Btn8_Click;
            btn9.Click += Btn9_Click;
            btn10.Click += Btn10_Click;
            return view;
        }

        private void Btn10_Click(object sender, EventArgs e)
        {
            Toast.MakeText(this.Activity, "Payment.........", ToastLength.Long).Show();

            //var trans2 = this.FragmentManager.BeginTransaction();
            //trans2.Replace(Resource.Id.content_frame, Payment.NewInstance(), "Payment");
            //trans2.AddToBackStack("Fragment1");
            //trans2.Commit();
        }

        private void Btn9_Click(object sender, EventArgs e)
        {
            Toast.MakeText(this.Activity, "Loading please wait.........", ToastLength.Long).Show();

            var trans2 = this.FragmentManager.BeginTransaction();
            trans2.Replace(Resource.Id.content_frame, Holdings.NewInstance(), "Holdings");
            trans2.AddToBackStack("Fragment1");
            trans2.Commit();

        }

        private void Btn8_Click(object sender, EventArgs e)
        {
            Toast.MakeText(this.Activity, "Loading please wait.........", ToastLength.Long).Show();

            var trans2 = this.FragmentManager.BeginTransaction();
            trans2.Replace(Resource.Id.content_frame, Actions.NewInstance(), "Statement");
            trans2.AddToBackStack("Fragment1");
            trans2.Commit();
        }

        private void Btn7_Click(object sender, EventArgs e)
        {
            Toast.MakeText(this.Activity, "Loading please wait.........", ToastLength.Long).Show();

            var trans2 = this.FragmentManager.BeginTransaction();
            trans2.Replace(Resource.Id.content_frame, Subscribe.NewInstance(), "Subscribe");
            trans2.AddToBackStack("Fragment1");
            trans2.Commit();
        }

        private void Btn6_Click(object sender, System.EventArgs e)
        {
            Toast.MakeText(this.Activity, "Loading please wait.........", ToastLength.Long).Show();

           var trans2 = this.FragmentManager.BeginTransaction();
            trans2.Replace(Resource.Id.content_frame, Price.NewInstance(), "Price");
            trans2.AddToBackStack("Fragment1");
            trans2.Commit();
        }

        private void Btn5_Click(object sender, System.EventArgs e)
        {
            Toast.MakeText(this.Activity, "Loading please wait.........", ToastLength.Long).Show();

            var trans2 = this.FragmentManager.BeginTransaction();
            trans2.Replace(Resource.Id.content_frame, Issuer.NewInstance(), "Issuer");
            trans2.AddToBackStack("Fragment1");
            trans2.Commit();
        }

        private void Btn1_Click(object sender, System.EventArgs e)
        {
            Toast.MakeText(this.Activity, "Loading please wait.........", ToastLength.Long).Show();

            var trans2 = this.FragmentManager.BeginTransaction();
            trans2.Replace(Resource.Id.content_frame, Escrow.NewInstance(), "Escrow");
            trans2.AddToBackStack("Fragment1");
            trans2.Commit();
        }

        private void Btn4_Click(object sender, System.EventArgs e)
        {
            Toast.MakeText(this.Activity, "Loading please wait.........", ToastLength.Long).Show();

            var trans2 = this.FragmentManager.BeginTransaction();
            trans2.Replace(Resource.Id.content_frame, TopLosers.NewInstance(), "TopLosers");
            trans2.AddToBackStack("Fragment1");
            trans2.Commit();
        }

        private void Btn3_Click(object sender, System.EventArgs e)
        {
            Toast.MakeText(this.Activity, "Loading please wait.........", ToastLength.Long).Show();

            var trans2 = this.FragmentManager.BeginTransaction();
            trans2.Replace(Resource.Id.content_frame,TopGainers.NewInstance(), "TopGainers");
            trans2.AddToBackStack("Fragment1");
            trans2.Commit();
        }

        private void Btn2_Click(object sender, System.EventArgs e)
        {
            Toast.MakeText(this.Activity, "Loading please wait.........", ToastLength.Long).Show();

            var trans2 = this.FragmentManager.BeginTransaction();
            trans2.Replace(Resource.Id.content_frame, Statement.NewInstance(), "Statement");
            trans2.AddToBackStack("Fragment1");
            trans2.Commit();
        }

    
    }
}