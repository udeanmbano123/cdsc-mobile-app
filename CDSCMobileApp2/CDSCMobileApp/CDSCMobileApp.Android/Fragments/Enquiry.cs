﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;
using RestSharp;

namespace CDSCMobileApp.Droid.Fragments
{
    public class Enquiry : Fragment
    {
        EditText enq, csd;
        Button btn;
        public static Enquiry NewInstance()
        {
            var frag1 = new Enquiry { Arguments = new Bundle() };
            return frag1;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);

            var ignored = base.OnCreateView(inflater, container, savedInstanceState);
            this.Activity.SetTitle(Resource.String.enquiry);
            Intent downloadIntent = new Intent(this.Activity, typeof(AppUsageIntent));
            UserActivity.title = "ENQUIRIES";
            // This is just one example of passing some values to an IntentService via the Intent:
            this.Activity.StartService(downloadIntent);
            View view = inflater.Inflate(Resource.Layout.Enquiry, null);
            enq = view.FindViewById<EditText>(Resource.Id.description);
            csd = view.FindViewById<EditText>(Resource.Id.csd);
            btn = view.FindViewById<Button>(Resource.Id.button1);
            csd.Text = UserActivity.member;
            csd.Enabled = false;
            btn.Click += Btn_Click;
            return view;
        }

        private void Btn_Click(object sender, EventArgs e)
        {
            if (enq.Text=="")
            {
                Toast.MakeText(this.Activity, "Enquiry is required", ToastLength.Short).Show();

            }
            else
            {
                Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this.Activity);


                string msg = enq.Text.Replace("+", "%20");
                var client = new RestClient(UserActivity.baseurl);
                var request = new RestRequest("ticketAdd/{feedback}/{mobile}", Method.POST);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request.AddUrlSegment("feedback",msg.Replace(".", ""));
                request.AddUrlSegment("mobile",csd.Text.Replace(" ",""));
               IRestResponse response = client.Execute(request);
                string validate = response.Content;
                validate = validate.Replace(@"""", "");
                string val = validate.Split(':')[0];
                alert.SetPositiveButton("OK", (senderAlert, args) => {
                    FragmentManager fm = this.FragmentManager;
                    //fm.PopBackStack(0,0);
                    var trans = fm.BeginTransaction();
                    fm.PopBackStack();
                });
                alert.SetTitle("Notification");
                alert.SetMessage("Thank you for your enquiry expect feedback from us very soon. An automated ticket has been generated " + validate.Split(':')[1].ToString());

                Android.App.Dialog dialog = alert.Create();
                dialog.Show(); enq.Text = "";
            }
        }
    }
}