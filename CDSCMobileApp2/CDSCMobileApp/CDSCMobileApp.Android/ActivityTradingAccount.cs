﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using Android.Support.V7.App;
using Android.Support.Design.Widget;
using Android.Widget;
using Android.App;
using Android.OS;
using System.Security.Cryptography;
using CDSCMobileApp.Activities;
using RestSharp;
using Android.Content;
using System.Net;
using System.IO;
using Android.Content.PM;

namespace CDSCMobileApp.Droid
{
    [Activity(Label = "MY ACCOUNT", Theme = "@style/AppTheme2", Icon = "@drawable/ctrademobile", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
    ScreenOrientation = ScreenOrientation.Portrait)]
    public class ActivityTradingAccount : AppCompatActivity
    {
        //int count = 1;
        Button button, button2, button3, button4;
        EditText username, pass;
        public static string baseUrl { get; set; }
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.ActivityTradingAccount);
            button = FindViewById<Button>(Resource.Id.btnLogin);
            button2 = FindViewById<Button>(Resource.Id.btnClear);
            button3 = FindViewById<Button>(Resource.Id.btnReg);
            button4 = FindViewById<Button>(Resource.Id.btnForg);
            username = FindViewById<EditText>(Resource.Id.username);
            pass = FindViewById<EditText>(Resource.Id.password);

              baseUrl = "http://197.248.8.104/CDSCW";
            

            button2.Click += delegate
            {

                username.Text = "";
                pass.Text = "";
            };

            button3.Click += delegate
            {
                StartActivity(typeof(AccountOpening));
            };

            button4.Click += delegate
            {
                StartActivity(typeof(ForgotActivity));
            };
            button.Click += LoginClick;
        }

        public void LoginClick(object sender, EventArgs e)
        {
            try 
	{	        
		string responJsonText = "";
            bool validA = username.Text.Replace(" ", "").All(c => Char.IsLetterOrDigit(c) || c.Equals('_'));

            if (username.Text == "")
            {
                Toast.MakeText(this, "Username is required", ToastLength.Long).Show();
                return;
            }
            else if (validA == false)
            {
                Toast.MakeText(this, "Username must be a letter , digit or underscore", ToastLength.Long).Show();
                return;
            }
            else if (pass.Text == "")
            {
                Toast.MakeText(this, "Password is required", ToastLength.Long).Show();
                return;
            }


            var client = new RestClient(baseUrl);
            var request = new RestRequest("LoginMem/{s}/{p}", Method.GET);
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("s", username.Text.Replace(" ", ""));
            request.AddUrlSegment("p", ComputeHash(pass.Text, new SHA256CryptoServiceProvider()));
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            validate = validate.Replace(@"""", "");
            string val = validate;
            if (validate=="")
            {
                validate = "0";
            }else
            {
                validate = "1";
            }

            if (validate == "0")
            {
                Toast.MakeText(this, "Wrong details please try again", ToastLength.Long).Show();
                return;
            }
            else if (validate == "1")
            {
               
                Toast.MakeText(this, "You are logged in", ToastLength.Long).Show();
                Finish();
                var activity2 = new Intent(this, typeof(UserActivity));
                activity2.PutExtra("Member", val);
                //activity2.PutExtra("Branch", "Lusaka");
                activity2.PutExtra("Username", username.Text);
                    if (val!="")
                    {
                  StartActivity(activity2);
                    }else
                    {
                 Toast.MakeText(this, "No account found associted with credentials", ToastLength.Long).Show();

                    }


                    return;
            }

	}
	catch (Exception f)
	{

                Toast.MakeText(this, "No internet or wifi", ToastLength.Short).Show();
                return;
            }
        }

        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = System.Text.Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }

        public bool IsAddressAvailable(string address)
        {

            Boolean isHttpOk;
            try
            {
                Stream sStream;
                HttpWebRequest urlReq;
                HttpWebResponse urlRes;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(address);
                req.Method = "HEAD";
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                isHttpOk = resp.StatusCode == HttpStatusCode.OK;
                resp.Close();

                return isHttpOk;
            }
            catch (Exception)
            {
                isHttpOk = false;
                return isHttpOk;
            }
        }


    }


}




