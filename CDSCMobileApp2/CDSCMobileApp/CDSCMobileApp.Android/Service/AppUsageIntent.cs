﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RestSharp;
using Newtonsoft.Json;
using System.IO;

namespace CDSCMobileApp.Droid
{
    [Service]
public class AppUsageIntent : IntentService
{

    public string path;
    const int NOTIFICATION_ID = 9009;
    public AppUsageIntent() : base("AppUsageIntent")
    {
    }

    protected override void OnHandleIntent(Android.Content.Intent intent)
    {
            var client = new RestClient(UserActivity.baseurl);
            var request = new RestRequest("AppUsage/{a}/{m}", Method.GET);
            request.JsonSerializer.ContentType = "application/json; charset=utf-8";
            request.AddUrlSegment("a", UserActivity.title);
            request.AddUrlSegment("m", UserActivity.member);
            IRestResponse response = client.Execute(request);
            string validate = response.Content;

            validate = validate.Replace(@"""", "");

        }

    }
}
