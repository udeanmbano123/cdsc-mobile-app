﻿using Android.Content;
using System;
using System.Collections.Generic;
using System.Text;

namespace CDSCMobileApp.Droid
{
    public class RefLayoutManager
    {
        ISharedPreferences sharePref;
        ISharedPreferencesEditor editor;
        Context context;

        // mode

        //shared preferene file name
        private static string pref_name = "Intro Slider";
        private static string is_first_time_lauch = "thefirst";



        public RefLayoutManager(Context context)
        {
            this.context = context;
            sharePref = this.context.GetSharedPreferences(pref_name, FileCreationMode.Private);
            editor = sharePref.Edit();
        }

        public void setFirstTimeLauch(bool isFirstTime)
        {
            editor.PutBoolean(is_first_time_lauch, isFirstTime);
            editor.Commit();

        }

        public Boolean isFirstTimeLauch()
        {
            return sharePref.GetBoolean(is_first_time_lauch, true);
        }

    }
}
