﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using CDSCMobileeApp.DROID.Fragments;
using Android.Graphics.Drawables;
using Android.Graphics;
using System.Xml.Serialization;
using System.IO;
using RestSharp;
using System.Net;
using CDSCMobileApp.DROID;
using Newtonsoft.Json;

namespace CDSCMobileApp.Droid
{
    [Activity(Label = "CDAs", Theme = "@style/AppTheme2", MainLauncher = false, Icon = "@drawable/ctrademobile")]
    public class ProdCDA : AppCompatActivity
    {
        RecyclerView neme;
        RecyclerView.LayoutManager mLayoutManager;
        ProdAdapter adapter;
        List<Productss> mPhotoAlbum;
        public static string baseurl;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.ActivityPriceNotification);
            
            baseurl = "http://197.248.8.104/CDSCW";

            neme = this.FindViewById<RecyclerView>(Resource.Id.recyclerView);
            mLayoutManager = new LinearLayoutManager(this);
            neme.SetLayoutManager(mLayoutManager);

            var client2 = new RestClient(baseurl);
            var request2 = new RestRequest("Subscribed/{s}", Method.GET);
            // request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };
            request2.AddUrlSegment("s", "0000000000450");
            IRestResponse response2 = client2.Execute(request2);
            // request.AddBody(new tblMeetings {  });
            string validate2 = response2.Content;


            List<Products> dataList2 = JsonConvert.DeserializeObject<List<Products>>(validate2);
            List<Productss> news = new List<Productss>();
            var dbsel2 = from s in dataList2
                         select s;

            int n = 0;
            foreach (var p in dbsel2)
            {
               
                news.Add(new Productss { id = n, Name = p.Name });
                n += 1;
            }

            Drawable[] imageId = new Drawable[news.Count];
            int x = 0;
            TextDrawable.TextDrawable drawable;
            foreach (var p in news)
            {
                //TextDrawable.TextDrawable drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BuildRound("ABC", GetRandomColor(), GetRandomColor());
                try
                {
                    drawable = TextDrawable.TextDrawable.TextDrwableBuilder.BeginConfig().FontSize(20).TextColor(Color.Black).Bold().EndConfig().BuildRound(p.Name.Substring(1), GetRandomColor(), GetRandomColor());

                    imageId[p.id] = drawable;
                }
                catch (Exception)
                {

                    continue;
                }
                
            }
            mPhotoAlbum = news;
            adapter = new ProdAdapter(mPhotoAlbum, imageId);
            neme.SetAdapter(adapter);

        }
        static Random rand = new Random();
        public static Color GetRandomColor()
        {
            int hue = rand.Next(255);
            Color color = Color.HSVToColor(
                new[] {
            hue,
            1.0f,
            1.0f,
                }
            );
            return color;
        }
        public bool IsAddressAvailable(string address)
        {

            Boolean isHttpOk;
            try
            {
                Stream sStream;
                HttpWebRequest urlReq;
                HttpWebResponse urlRes;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(address);
                req.Method = "HEAD";
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                isHttpOk = resp.StatusCode == HttpStatusCode.OK;
                resp.Close();

                return isHttpOk;
            }
            catch (Exception)
            {
                isHttpOk = false;
                return isHttpOk;
            }
        }
    }
}