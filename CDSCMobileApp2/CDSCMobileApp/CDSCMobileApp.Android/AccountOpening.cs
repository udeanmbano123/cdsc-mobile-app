﻿using System;
using System.Linq;

using Android.App;
using Android.OS;
using Android.Widget;
using Android.Support.V7.App;
using System.Security.Cryptography;
using RestSharp;
using System.Net;
using System.IO;
using Android.Content.PM;

namespace CDSCMobileApp.Droid
{
    [Activity(Label = "ACCOUNT OPENING", Theme = "@style/AppTheme2", Icon = "@drawable/ctrademobile", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation,
    ScreenOrientation = ScreenOrientation.Portrait)]
    public class AccountOpening : AppCompatActivity
    {
        Button button, button2, button3;
        EditText username, member, pass, pass2,email;
        public static string baseUrl { get; set; }
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.AccountOpening);

               baseUrl = "http://197.248.8.104/CDSCW";
           
            button = FindViewById<Button>(Resource.Id.btnLogin);
            button2 = FindViewById<Button>(Resource.Id.btnClear);
            button3 = FindViewById<Button>(Resource.Id.btnBack);
            username = FindViewById<EditText>(Resource.Id.username);
            pass = FindViewById<EditText>(Resource.Id.password);
            pass2 = FindViewById<EditText>(Resource.Id.confirmpassword);
            member = FindViewById<EditText>(Resource.Id.member);
            email = FindViewById<EditText>(Resource.Id.email);

            button2.Click += delegate
            {

                username.Text = "";
                pass.Text = "";
                username.Text = "";
                member.Text = "";
                pass.Text = "";
                pass2.Text = "";
                //email.Text = "";
            };

            button3.Click += delegate
            {

                StartActivity(typeof(ActivityTradingAccount));
            };
            button.Click += LoginClick;
        }

        public void LoginClick(object sender, EventArgs e)
        {
             try 
	{
               baseUrl = "http://197.248.8.104/CDSCW";
                
                bool validA = username.Text.Replace(" ", "").All(c => Char.IsLetterOrDigit(c) || c.Equals('_'));
            if (username.Text == "")
            {
                Toast.MakeText(this, "Username  is required", ToastLength.Short).Show();
                return;

            }
            else if (validA == false)
            {
                Toast.MakeText(this, "Username must be a letter , digit or underscore", ToastLength.Short).Show();
                return;

            }
            else if (member.Text == "")
            {
                Toast.MakeText(this, "Client number  is required", ToastLength.Short).Show();
                return;

            }
            else if (email.Text == "")
            {
                Toast.MakeText(this, "Email is required", ToastLength.Short).Show();
                return;

            }
            else if (pass.Text == "")
            {
                Toast.MakeText(this, "Password is required", ToastLength.Short).Show();
                return;

            }
            else if (pass2.Text == "")
            {
                Toast.MakeText(this, "Confirm password  is required", ToastLength.Short).Show();
                return;

            }
            else if (pass.Text != pass2.Text)
            {
                Toast.MakeText(this, "Passwords  do not match", ToastLength.Short).Show();
                return;

            }
            else
            {
                button.Enabled = false;
                var client = new RestClient(baseUrl);
                var request = new RestRequest("Register/{username}/{password}/{email}/{membernumber}", Method.POST);
                request.JsonSerializer.ContentType = "application/json; charset=utf-8";
                request.AddUrlSegment("username", username.Text.ToString().Replace(" ", ""));
                request.AddUrlSegment("password", ComputeHash(pass.Text, new SHA256CryptoServiceProvider()));
                request.AddUrlSegment("email", email.Text.ToString().Replace("*", "-"));
                request.AddUrlSegment("membernumber", member.Text.ToString().Replace(" ", ""));
                IRestResponse response = client.Execute(request);
                string validate = response.Content;


                validate = validate.Replace(@"""", "");
                string val = validate;
                if (val == "Successfully registered")
                {
                    //Toast.MakeText(this, "Registration was succesful", ToastLength.Short).Show();
                    Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(this);
                    Android.App.AlertDialog alertDialog = builder.Create();
                    alertDialog.SetTitle("Notification");
                    alertDialog.SetIcon(Resource.Drawable.ic_launcher);
                    alertDialog.SetMessage("Registration was successful");


                    alertDialog.SetButton("OK", (s, ev) =>
                    {
                        //DO Something


                    });
                    alertDialog.Show();
                }
                else
                {
                    //Toast.MakeText(this, "Registration was unsuccesful", ToastLength.Short).Show();
                    Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(this);
                    Android.App.AlertDialog alertDialog = builder.Create();
                    alertDialog.SetTitle("Notification");
                    alertDialog.SetIcon(Resource.Drawable.ic_launcher);
                    alertDialog.SetMessage("Registration was unsuccessful");


                    alertDialog.SetButton("OK", (s, ev) =>
                    {
                        //DO Something


                    });
                    alertDialog.Show();
                }


                username.Text = "";
                pass.Text = "";
                username.Text = "";
                member.Text = "";
                pass.Text = "";
                pass2.Text = "";
                //email.Text = "";
            }
	}
	catch (Exception f)
	{

                Toast.MakeText(this, "No internet or wifi", ToastLength.Short).Show();
                return;
            }

        }

        public string ComputeHash(string input, HashAlgorithm algorithm)
        {
            Byte[] inputBytes = System.Text.Encoding.UTF8.GetBytes(input);

            Byte[] hashedBytes = algorithm.ComputeHash(inputBytes);

            return BitConverter.ToString(hashedBytes);
        }
        public bool IsAddressAvailable(string address)
        {

            Boolean isHttpOk;
            try
            {
                Stream sStream;
                HttpWebRequest urlReq;
                HttpWebResponse urlRes;

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(address);
                req.Method = "HEAD";
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                isHttpOk = resp.StatusCode == HttpStatusCode.OK;
                resp.Close();

                return isHttpOk;
            }
            catch (Exception)
            {
                isHttpOk = false;
                return isHttpOk;
            }
        }
    }
}

