package com.service;

import java.io.File; 
import java.io.FileInputStream; 
import java.io.FileNotFoundException;  
import java.io.FileOutputStream; 
import java.io.IOException; 
import java.io.ObjectInputStream; 
import java.io.ObjectOutputStream; 
import java.util.ArrayList; 
import java.util.List;  
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.TimeZone;

public class UserDAO {  
	
        
   public List<User> getAllUsers(){ 
      
      List<User> userList =new ArrayList<User>(); 
    userList.add(new User(1, "Udean Mbano", "Java Developer"));
      userList.add(new User(2, "Mbano Mbano", "Android Developer"));
      userList.add(new User(3, "Mike Mbano", "C# Developer"));
      userList.add(new User(4, "File Mbano", "VB Developer"));
      
      return userList; 
   } 
   public List<DATASET_CDA> getAllDATASET_CDA(){ 
	      
	      List<DATASET_CDA> userList =new ArrayList<DATASET_CDA>(); 
	      
	      try {
			String url = "jdbc:oracle:thin:@192.168.100.10:1521:TESTDB";
			//properties for creating connection to Oracle database
			Properties props = new Properties();
			props.setProperty("user", "CDSC_ESCROW");
			props.setProperty("password", "ESCROW#12");
	TimeZone timeZone = TimeZone.getTimeZone("Nairobi");

			TimeZone.setDefault(timeZone);
			//creating connection to Oracle database using JDBC
			Class.forName("oracle.jdbc.OracleDriver");
			Connection conn = DriverManager.getConnection(url, props);
			System.out.println("Connected to oracle \n");
			// String sql ="select sysdate as current_day from dual";
			String sql = "SELECT\n" + "	MEMBER_CODE,\n" + "	MEMBER_TYPE,\n" + "	NAME,\n" + "	ADDRESS1,\n"
					+ "	ADDRESS2,\n" + "	ADDRESS3,\n" + "	TOWN,\n" + "	POST_CODE,\n" + "	COUNTRY_CODE,\n"
					+ "	TELEPHONE,\n" + "	fax,\n" + "	E_MAIL,\n" + "	STATUS,\n" + "	status_description\n" + "FROM\n"
					+ "	CDSC.DATASET_CDA where RowNum<=10";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				userList.add(new DATASET_CDA(result.getString("MEMBER_CODE"),result.getString("MEMBER_TYPE"),result.getString("NAME"),result.getString("ADDRESS1"),result.getString("ADDRESS2"),
						result.getString("ADDRESS3"),result.getString("TOWN"), result.getString("POST_CODE"),result.getString("COUNTRY_CODE"), result.getString("TELEPHONE"),result.getString("fax"),
						result.getString("E_MAIL"), result.getString("STATUS"), result.getString("status_description")));
			

			
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			userList.add(new DATASET_CDA(e.toString(),e.toString(),e.toString(),e.toString(),e.toString(),
					e.toString(),e.toString(), e.toString(),e.toString(),e.toString(),e.toString(),
					e.toString(), e.toString(),e.toString()));
		}
		return userList; 
	   } 
    
   public List<DATASET_HOLDINGS> getAllDATASET_HOLD(){ 
	      
	      List<DATASET_HOLDINGS> userList =new ArrayList<DATASET_HOLDINGS>(); 
	      try {
			String url = "jdbc:oracle:thin:@192.168.100.10:1521:TESTDB";
			//properties for creating connection to Oracle database
			Properties props = new Properties();
			props.setProperty("user", "CDSC_ESCROW");
			props.setProperty("password", "ESCROW#12");
	TimeZone timeZone = TimeZone.getTimeZone("Nairobi");

			TimeZone.setDefault(timeZone);
			//creating connection to Oracle database using JDBC
			Class.forName("oracle.jdbc.OracleDriver");
			Connection conn = DriverManager.getConnection(url, props);
			System.out.println("Connected to oracle \n");
			// String sql ="select sysdate as current_day from dual";
			String sql = "select CLIENT_PREFIX,CLIENT_SUFFIX,MEMBER_CODE,MEMBER_TYPE,SHORT_NAME,ISSUER_CODE,MAIN_TYPE,\r\n" + 
					"SUB_TYPE,qnty_held,DATE_LAST_TRANSACTION, sec_ACC_TYPE\r\n" + 
					"from CDSC.DATASET_HOLDINGS where RowNum<=10";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				userList.add(new DATASET_HOLDINGS(result.getString("CLIENT_PREFIX"),result.getString("CLIENT_SUFFIX"),result.getString("MEMBER_CODE"),result.getString("MEMBER_TYPE"),result.getString("SHORT_NAME"),
						result.getString("ISSUER_CODE"),result.getString("MAIN_TYPE"), result.getString("SUB_TYPE"),result.getString("qnty_held"), result.getString("DATE_LAST_TRANSACTION"),result.getString("sec_ACC_TYPE")));
			

			
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			userList.add(new DATASET_HOLDINGS(e.toString(),e.toString(),e.toString(),e.toString(),e.toString(),
					e.toString(),e.toString(), e.toString(),e.toString(),e.toString(),e.toString()));
		}
	      return userList; 
	   } 
   
   public List<DATASET_ISSUERS> getAllDATASET_ISSUERS(){ 
	      
	      List<DATASET_ISSUERS> userList =new ArrayList<DATASET_ISSUERS>(); 
	      try {
			String url = "jdbc:oracle:thin:@192.168.100.10:1521:TESTDB";
			//properties for creating connection to Oracle database
			Properties props = new Properties();
			props.setProperty("user", "CDSC_ESCROW");
			props.setProperty("password", "ESCROW#12");
	TimeZone timeZone = TimeZone.getTimeZone("Nairobi");

			TimeZone.setDefault(timeZone);
			//creating connection to Oracle database using JDBC
			Class.forName("oracle.jdbc.OracleDriver");
			Connection conn = DriverManager.getConnection(url, props);
			System.out.println("Connected to oracle \n");
			// String sql ="select sysdate as current_day from dual";
			String sql = "select\r\n" + 
					" NAME,ISSUER_CODE,MAIN_TYPE,SUB_TYPE,ISIN,SHORT_NAME, FH_LIMIT,FOREIGN_HOLDING,PRICE_LAST_TRADED,COUNTRY_OF_INITIAL_ISSUE,\r\n" + 
					" PAR_VALUE,DATE_LAST_TRADED,QTY_ISSUED,qty_demmarted,PREVIOUS_CLOSE,INDEXED_PRICE,PREVIOUS_INDEXED_PRICE,DATE_LAST_INDEXED,INDEXED_QUANTITY \r\n" + 
					" ,AVERAGE_PRICE,SECURITY_CATEGORY,REFERENCE_PRICE, STATUS,status_description\r\n" + 
					"from CDSC.DATASET_ISSUERS where RowNum<=10";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				userList.add(new DATASET_ISSUERS(result.getString("NAME"),result.getString("ISSUER_CODE"),result.getString("MAIN_TYPE"),result.getString("SUB_TYPE"),result.getString("ISIN"),result.getString("SHORT_NAME"),result.getString("FH_LIMIT"),result.getString("FOREIGN_HOLDING"),result.getString("PRICE_LAST_TRADED"),result.getString("COUNTRY_OF_INITIAL_ISSUE"),
						result.getString("PAR_VALUE"),result.getString("DATE_LAST_TRADED"),result.getString("QTY_ISSUED"),result.getString("qty_demmarted"),result.getString("PREVIOUS_CLOSE"),result.getString("INDEXED_PRICE"),result.getString("PREVIOUS_INDEXED_PRICE"),result.getString("DATE_LAST_INDEXED"),result.getString("INDEXED_QUANTITY"),result.getString("AVERAGE_PRICE"),result.getString("SECURITY_CATEGORY"),result.getString("REFERENCE_PRICE"), result.getString("STATUS"),result.getString("status_description")));
			

			
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			userList.add(new DATASET_ISSUERS(e.toString(),e.toString(),e.toString(),e.toString(),e.toString(),
					e.toString(),e.toString(), e.toString(),e.toString(),e.toString(),e.toString(), null, null, null, null, null, null, null, null, null, null, null, null, null));
		}

	
	      return userList; 
	   }
   
   public List<DATASET_PRICES> getAllDATASET_PRICES(){ 
	      
	      List<DATASET_PRICES> userList =new ArrayList<DATASET_PRICES>(); 
	      try {
			String url = "jdbc:oracle:thin:@192.168.100.10:1521:TESTDB";
			//properties for creating connection to Oracle database
			Properties props = new Properties();
			props.setProperty("user", "CDSC_ESCROW");
			props.setProperty("password", "ESCROW#12");
	TimeZone timeZone = TimeZone.getTimeZone("Nairobi");

			TimeZone.setDefault(timeZone);
			//creating connection to Oracle database using JDBC
			Class.forName("oracle.jdbc.OracleDriver");
			Connection conn = DriverManager.getConnection(url, props);
			System.out.println("Connected to oracle \n");
			// String sql ="select sysdate as current_day from dual";
			String sql = "select Trade_date,ISSUER_CODE,ISIN, MAIN_TYPE,SUB_TYPE,PRICE_HI,\r\n" + 
					"PRICE_LOW,CLOSE_PRICE,AVERAGE_PRICE,SHARE_VOLUME,num_of_trades,TURNOVER \r\n" + 
					"from CDSC.DATASET_PRICES where RowNum<=10";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				userList.add(new DATASET_PRICES(result.getString("Trade_date"),result.getString("ISSUER_CODE"),result.getString("ISIN"), result.getString("MAIN_TYPE"),result.getString("SUB_TYPE"),result.getString("PRICE_HI"),
						result.getString("PRICE_LOW"),result.getString("CLOSE_PRICE"),result.getString("AVERAGE_PRICE"),result.getString("SHARE_VOLUME"),result.getString("num_of_trades"),result.getString("TURNOVER") ));
			

			
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			userList.add(new DATASET_PRICES(e.toString(),e.toString(),e.toString(),e.toString(),e.toString(),
					e.toString(),e.toString(), e.toString(),e.toString(),e.toString(),e.toString(), null));
		}
	      return userList; 
	   } 
   public List<ESCROW_CLIENTS> getAllESCROW_CLIENTS(String id){ 
	      
	      List<ESCROW_CLIENTS> userList =new ArrayList<ESCROW_CLIENTS>(); 
	      try {
			String url = "jdbc:oracle:thin:@192.168.100.10:1521:TESTDB";
			//properties for creating connection to Oracle database
			Properties props = new Properties();
			props.setProperty("user", "CDSC_ESCROW");
			props.setProperty("password", "ESCROW#12");
	TimeZone timeZone = TimeZone.getTimeZone("Nairobi");

			TimeZone.setDefault(timeZone);
			//creating connection to Oracle database using JDBC
			Class.forName("oracle.jdbc.OracleDriver");
			Connection conn = DriverManager.getConnection(url, props);
			System.out.println("Connected to oracle \n");
			// String sql ="select sysdate as current_day from dual";
			String sql = "SELECT MEMBER_CODE, MEMBER_TYPE,CLIENT_PREFIX, CLIENT_SUFFIX, CLIENT_TYPE,\r\n" + 
					"    SURNAME,OTHER_NAMES,DATE_OF_REGISTRATION,DATE_CHANGED\r\n" + 
					" FROM  CDSC.ESCROW_CLIENTS where CLIENT_PREFIX ='"+ id +"'";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				userList.add(new ESCROW_CLIENTS(result.getString("MEMBER_CODE"),result.getString("MEMBER_TYPE"),result.getString("CLIENT_PREFIX"),result.getString("CLIENT_SUFFIX"),result.getString("CLIENT_TYPE"),
						result.getString("SURNAME"),result.getString("OTHER_NAMES"),result.getString("DATE_OF_REGISTRATION"),result.getString("DATE_CHANGED") ));
			

			
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			userList.add(new ESCROW_CLIENTS(e.toString(),e.toString(),e.toString(),e.toString(),e.toString(),
					e.toString(),e.toString(), e.toString(),e.toString()));
		}
	      return userList; 
	   } 
   
  
   public List<ESCROW_MINI_STMT> getAllESCROW_MINI_STMT(String id){ 
	      
	      List<ESCROW_MINI_STMT> userList =new ArrayList<ESCROW_MINI_STMT>(); 
	      try {
			String url = "jdbc:oracle:thin:@192.168.100.10:1521:TESTDB";
			//properties for creating connection to Oracle database
			Properties props = new Properties();
			props.setProperty("user", "CDSC_ESCROW");
			props.setProperty("password", "ESCROW#12");
	TimeZone timeZone = TimeZone.getTimeZone("Nairobi");

			TimeZone.setDefault(timeZone);
			//creating connection to Oracle database using JDBC
			Class.forName("oracle.jdbc.OracleDriver");
			Connection conn = DriverManager.getConnection(url, props);
			System.out.println("Connected to oracle \n");
			// String sql ="select sysdate as current_day from dual";
			String sql = "SELECT CLIENT_PREFIX, MEMBER_CODE,  ISSUER_CODE, MAIN_TYPE, description, date_system, tshares\r\n" + 
					"From CDSC.ESCROW_MINI_STMT where CLIENT_PREFIX = '"+ id +"'";
			//creating PreparedStatement object to execute query
			PreparedStatement preStatement = conn.prepareStatement(sql);
			ResultSet result = preStatement.executeQuery();
			while (result.next()) {
				userList.add(new ESCROW_MINI_STMT(result.getString("CLIENT_PREFIX"),result.getString("MEMBER_CODE"),result.getString("ISSUER_CODE"),result.getString("MAIN_TYPE"),result.getString("description"),result.getString("date_system"),result.getString("tshares")));
			

			
			} 
		} catch (SQLException | ClassNotFoundException e) {
			// TODO: handle exception
			userList.add(new ESCROW_MINI_STMT(e.toString(),e.toString(),e.toString(),e.toString(),e.toString(),
					e.toString(),e.toString()));
		}
	      return userList; 
	   }    
   
}
