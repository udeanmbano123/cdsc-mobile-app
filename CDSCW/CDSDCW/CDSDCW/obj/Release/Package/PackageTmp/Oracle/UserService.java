package com.service;

import java.util.List; 
import javax.ws.rs.GET; 
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces; 
import javax.ws.rs.core.MediaType;  
@Path("/UserService") 

public class UserService {  
   UserDAO userDao = new UserDAO();   
   @GET 
   @Path("/test/{id}/{id2}") 
   @Produces(MediaType.TEXT_HTML) 
   public String test(@PathParam("id") String id,@PathParam("id2") String id2){ 
      return "You requested "+ id + " marry " + id2; 
   }  
   
   @GET 
   @Path("/users") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<User> getUsers(){ 
      return userDao.getAllUsers(); 
   }  
   
   @GET 
   @Path("/dtcda") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<DATASET_CDA> getDATASETCDA(){ 
      return userDao.getAllDATASET_CDA(); 
   }  
   
   @GET 
   @Path("/dthold") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<DATASET_HOLDINGS> getDATASETHOLD(){ 
      return userDao.getAllDATASET_HOLD(); 
   } 
   
   @GET 
   @Path("/dtissue") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<DATASET_ISSUERS> getDATASETISSUERS(){ 
      return userDao.getAllDATASET_ISSUERS(); 
   } 
   @GET 
   @Path("/dtprices") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<DATASET_PRICES> getDATASETPRICES(){ 
      return userDao.getAllDATASET_PRICES(); 
   } 
   
   @GET 
   @Path("/dtclients/{account}") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<ESCROW_CLIENTS> getESCROWCLIENTS(@PathParam("account") String account){ 
      return userDao.getAllESCROW_CLIENTS(account); 
   } 
   
   @GET 
   @Path("/dtstmt/{account}") 
   @Produces(MediaType.APPLICATION_XML) 
   public List<ESCROW_MINI_STMT> getDATASETESCROW_MINI_STMT(@PathParam("account") String account){ 
      return userDao.getAllESCROW_MINI_STMT(account); 
   } 
   
}
