package com.service;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "DATASETHOLDINGS") 
public class DATASET_ISSUERS implements Serializable {  
	   private static final long serialVersionUID = 1L; 
	   public DATASET_ISSUERS() {}
	 public String getNAME() {
		return NAME;
	}
	 @XmlElement 
	public void setNAME(String nAME) {
		NAME = nAME;
	}
	public String getISSUER_CODE() {
		return ISSUER_CODE;
	}
	 @XmlElement 
	public void setISSUER_CODE(String iSSUER_CODE) {
		ISSUER_CODE = iSSUER_CODE;
	}
	public String getMAIN_TYPE() {
		return MAIN_TYPE;
	}
	 @XmlElement 
	public void setMAIN_TYPE(String mAIN_TYPE) {
		MAIN_TYPE = mAIN_TYPE;
	}
	public String getSUB_TYPE() {
		return SUB_TYPE;
	}
	 @XmlElement 
	public void setSUB_TYPE(String sUB_TYPE) {
		SUB_TYPE = sUB_TYPE;
	}
	public String getISIN() {
		return ISIN;
	}
	 @XmlElement 
	public void setISIN(String iSIN) {
		ISIN = iSIN;
	}
	public String getSHORT_NAME() {
		return SHORT_NAME;
	}
	 @XmlElement 
	public void setSHORT_NAME(String sHORT_NAME) {
		SHORT_NAME = sHORT_NAME;
	}
	public String getFH_LIMIT() {
		return FH_LIMIT;
	}
	 @XmlElement 
	public void setFH_LIMIT(String fH_LIMIT) {
		FH_LIMIT = fH_LIMIT;
	}
	public String getFOREIGN_HOLDING() {
		return FOREIGN_HOLDING;
	}
	 @XmlElement 
	public void setFOREIGN_HOLDING(String fOREIGN_HOLDING) {
		FOREIGN_HOLDING = fOREIGN_HOLDING;
	}
	public String getPRICE_LAST_TRADED() {
		return PRICE_LAST_TRADED;
	}
	 @XmlElement 
	public void setPRICE_LAST_TRADED(String pRICE_LAST_TRADED) {
		PRICE_LAST_TRADED = pRICE_LAST_TRADED;
	}
	public String getCOUNTRY_OF_INITIAL_ISSUE() {
		return COUNTRY_OF_INITIAL_ISSUE;
	}
	 @XmlElement 
	public void setCOUNTRY_OF_INITIAL_ISSUE(String cOUNTRY_OF_INITIAL_ISSUE) {
		COUNTRY_OF_INITIAL_ISSUE = cOUNTRY_OF_INITIAL_ISSUE;
	}
	public String getPAR_VALUE() {
		return PAR_VALUE;
	}
	 @XmlElement 
	public void setPAR_VALUE(String pAR_VALUE) {
		PAR_VALUE = pAR_VALUE;
	}
	public String getDATE_LAST_TRADED() {
		return DATE_LAST_TRADED;
	}
	 @XmlElement 
	public void setDATE_LAST_TRADED(String dATE_LAST_TRADED) {
		DATE_LAST_TRADED = dATE_LAST_TRADED;
	}
	public String getQTY_ISSUED() {
		return QTY_ISSUED;
	}
	 @XmlElement 
	public void setQTY_ISSUED(String qTY_ISSUED) {
		QTY_ISSUED = qTY_ISSUED;
	}
	public String getQty_demmarted() {
		return qty_demmarted;
	} @XmlElement
	public void setQty_demmarted(String qty_demmarted) {
		this.qty_demmarted = qty_demmarted;
	}
	 
	public String getPREVIOUS_CLOSE() {
		return PREVIOUS_CLOSE;
	}
	 @XmlElement 
	public void setPREVIOUS_CLOSE(String pREVIOUS_CLOSE) {
		PREVIOUS_CLOSE = pREVIOUS_CLOSE;
	}
	public String getINDEXED_PRICE() {
		return INDEXED_PRICE;
	}
	 @XmlElement 
	public void setINDEXED_PRICE(String iNDEXED_PRICE) {
		INDEXED_PRICE = iNDEXED_PRICE;
	}
	public String getPREVIOUS_INDEXED_PRICE() {
		return PREVIOUS_INDEXED_PRICE;
	}
	 @XmlElement 
	public void setPREVIOUS_INDEXED_PRICE(String pREVIOUS_INDEXED_PRICE) {
		PREVIOUS_INDEXED_PRICE = pREVIOUS_INDEXED_PRICE;
	}
	public String getDATE_LAST_INDEXED() {
		return DATE_LAST_INDEXED;
	}
	 @XmlElement 
	public void setDATE_LAST_INDEXED(String dATE_LAST_INDEXED) {
		DATE_LAST_INDEXED = dATE_LAST_INDEXED;
	}
	public String getINDEXED_QUANTITY() {
		return INDEXED_QUANTITY;
	}
	 @XmlElement 
	public void setINDEXED_QUANTITY(String iNDEXED_QUANTITY) {
		INDEXED_QUANTITY = iNDEXED_QUANTITY;
	}
	public String getAVERAGE_PRICE() {
		return AVERAGE_PRICE;
	}
	 @XmlElement 
	public void setAVERAGE_PRICE(String aVERAGE_PRICE) {
		AVERAGE_PRICE = aVERAGE_PRICE;
	}
	public String getSECURITY_CATEGORY() {
		return SECURITY_CATEGORY;
	}
	 @XmlElement 
	public void setSECURITY_CATEGORY(String sECURITY_CATEGORY) {
		SECURITY_CATEGORY = sECURITY_CATEGORY;
	}
	public String getREFERENCE_PRICE() {
		return REFERENCE_PRICE;
	}
	 @XmlElement 
	public void setREFERENCE_PRICE(String rEFERENCE_PRICE) {
		REFERENCE_PRICE = rEFERENCE_PRICE;
	}
	public String getSTATUS() {
		return STATUS;
	}
	 @XmlElement 
	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}
	public String getStatus_description() {
		return status_description;
	}
	 @XmlElement 
	public void setStatus_description(String status_description) {
		this.status_description = status_description;
	}
	public DATASET_ISSUERS(String nAME, String iSSUER_CODE, String mAIN_TYPE, String sUB_TYPE, String iSIN,
			String sHORT_NAME, String fH_LIMIT, String fOREIGN_HOLDING, String pRICE_LAST_TRADED,
			String cOUNTRY_OF_INITIAL_ISSUE, String pAR_VALUE, String dATE_LAST_TRADED, String qTY_ISSUED,
			String qty_demmarted, String pREVIOUS_CLOSE, String iNDEXED_PRICE, String pREVIOUS_INDEXED_PRICE,
			String dATE_LAST_INDEXED, String iNDEXED_QUANTITY, String aVERAGE_PRICE, String sECURITY_CATEGORY,
			String rEFERENCE_PRICE, String sTATUS, String status_description) {
		super();
		NAME = nAME;
		ISSUER_CODE = iSSUER_CODE;
		MAIN_TYPE = mAIN_TYPE;
		SUB_TYPE = sUB_TYPE;
		ISIN = iSIN;
		SHORT_NAME = sHORT_NAME;
		FH_LIMIT = fH_LIMIT;
		FOREIGN_HOLDING = fOREIGN_HOLDING;
		PRICE_LAST_TRADED = pRICE_LAST_TRADED;
		COUNTRY_OF_INITIAL_ISSUE = cOUNTRY_OF_INITIAL_ISSUE;
		PAR_VALUE = pAR_VALUE;
		DATE_LAST_TRADED = dATE_LAST_TRADED;
		QTY_ISSUED = qTY_ISSUED;
		this.qty_demmarted = qty_demmarted;
		PREVIOUS_CLOSE = pREVIOUS_CLOSE;
		INDEXED_PRICE = iNDEXED_PRICE;
		PREVIOUS_INDEXED_PRICE = pREVIOUS_INDEXED_PRICE;
		DATE_LAST_INDEXED = dATE_LAST_INDEXED;
		INDEXED_QUANTITY = iNDEXED_QUANTITY;
		AVERAGE_PRICE = aVERAGE_PRICE;
		SECURITY_CATEGORY = sECURITY_CATEGORY;
		REFERENCE_PRICE = rEFERENCE_PRICE;
		STATUS = sTATUS;
		this.status_description = status_description;
	}
	private String NAME;
	 private String ISSUER_CODE;
	 private String MAIN_TYPE;
	 private String SUB_TYPE;
	 private String ISIN;
	 private String SHORT_NAME;
	 private String FH_LIMIT;
	 private String FOREIGN_HOLDING;
	 private String PRICE_LAST_TRADED;
	 private String COUNTRY_OF_INITIAL_ISSUE;
	 private String PAR_VALUE;
	 private String DATE_LAST_TRADED;
	 private String QTY_ISSUED;
	 private String  qty_demmarted;
	 private String PREVIOUS_CLOSE;
	 private String INDEXED_PRICE;
	 private String PREVIOUS_INDEXED_PRICE;
	 private String DATE_LAST_INDEXED;
	 private String INDEXED_QUANTITY;
	 private String AVERAGE_PRICE;
	 private String SECURITY_CATEGORY;
	 private String REFERENCE_PRICE;
	 private String STATUS;
	 private String status_description;
	 
	 
}
